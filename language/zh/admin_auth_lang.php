<?php

$lang['auth_required']           = '你現正嘗試存取受保護區位。請先登入';
$lang['auth_loginid']            = '登入ID';
$lang['auth_password']           = '密碼';
$lang['auth_loginid_notfound']   = '登入 ID 不存在。請檢查資料是否正確，然後再試';
$lang['auth_password_incorrect'] = '密碼不正確，請重新輸入';
$lang['auth_unknown_error']      = '未知的資料，請聯絡你的技術支援以取得協助';

$lang['profile_password_change']                = '更改密碼';
$lang['profile_password_change_success']        = '成功更改密碼';
$lang['profile_password_change_fail']           = '對不起, 無法更改密碼';
$lang['profile_password_old']                   = '現時密碼';
$lang['profile_password_current_description']   = '你需要輸入現時使用的密碼以更改新密碼';
$lang['profile_password_old_incorrect']         = '密碼不正確，請重新輸入';
$lang['profile_password_new']                   = '新密碼';
$lang['profile_password_description']           = '請填寫你的新密碼，最少 %d 位英文或數字';
$lang['profile_password_error_invalid']         = '密碼格式不正確';
$lang['profile_password_error_same']            = '請勿填寫現時的密碼作為你的新密碼';
$lang['profile_password_retype']                = '重填新密碼';
$lang['profile_password_retype_description']    = '請重新輸入你的新密碼';
$lang['profile_password_retype_error_notmatch'] = '新密碼不符，請重新輸入';