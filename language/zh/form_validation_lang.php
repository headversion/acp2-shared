<?php

$lang['required']			= "%s 欄位是必須的";
$lang['isset']				= "%s 欄位不可為空。";
$lang['valid_email']		= "%s 欄位必須包含一個有效的電子郵件地址。";
$lang['valid_emails']		= "%s 欄位必須包含所有有效的電子郵件地址。";
$lang['valid_url']			= "%s 欄位必須包含一個有效的URL.";
$lang['valid_ip']			= "%s 欄位必須包含一個有效的IP地址.";
$lang['min_length']			= "%s 欄位的內容必須至少為 %s 個字。";
$lang['max_length']			= "%s 欄位的內容不能超過 %s 個字。";
$lang['exact_length']		= "%s 欄位的內容必須是長度正好 %s 個字。";
$lang['alpha']				= "%s 欄位只能包含英文字母。";
$lang['alpha_numeric']		= "%s 欄位只能包含英文字母或數字。";
$lang['alpha_dash']			= "%s 欄位只能包含字母數字，下劃線和破折號。";
$lang['numeric']			= "%s 欄位只能包含數字。";
$lang['is_numeric']			= "%s 欄位只能包含數字。";
$lang['integer']			= "%s 欄位必須包含一個整數。";
$lang['regex_match']		= "%s 欄位是不正確的格式。";
$lang['matches']			= "%s 欄位與 %s 欄位並不匹配。";
$lang['is_natural']			= "%s 欄位只能包含正數。";
$lang['is_natural_no_zero']	= "%s 欄位必須是一個大於零的數字。";
$lang['decimal']			= "%s 欄位必須是一個十進制數。";
$lang['less_than']			= "%s 欄位必須是一個數字小於 %s 的數字.";
$lang['greater_than']		= "%s 欄位必須是一個大於 %s 的數字.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */