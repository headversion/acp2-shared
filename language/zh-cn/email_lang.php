<?php
   $lang['email_must_be_array'] = "电子邮件必须是是阵列。";
   $lang['email_invalid_address'] = "不正確電郵地址: %s";
   $lang['email_attachment_missing'] = "沒法找到以下電郵地址的附屬物: %s";
   $lang['email_attachment_unreadable'] = "沒法打開附屬物: %s";
   $lang['email_no_recipients'] = "你必須填上接收人: To, Cc, or Bcc";
   $lang['email_send_failure_phpmail'] = "沒法利用PHP mail() 寄出電郵地址.你的伺服器可能不接立以下的傳送方式.";
   $lang['email_send_failure_sendmail'] = "沒法利用PHP Sendmail 寄出電郵地址.你的伺服器可能不接立以下的傳送方式";
   $lang['email_send_failure_smtp'] = "沒法利用PHP SMTP 寄出電郵地址.你的伺服器可能不接立以下的傳送方式.";
   $lang['email_sent'] = "你的信息已經成功寄出: %s";
   $lang['email_no_socket'] = "沒法利用socket 寄出電郵地址.請檢查設定";
   $lang['email_no_hostname'] = "你沒有設定SMTP hostname主機名稱.";
   $lang['email_smtp_error'] = "請留意以下SMTP的錯誤: %s";
   $lang['email_no_smtp_unpw'] = "錯誤:你必須定義SMTP 的用戶名稱以及密碼.";
   $lang['email_failed_smtp_login'] = "傳送auth 登入失敗. 錯誤: %s";
   $lang['email_smtp_auth_un'] = "鑑定用戶名稱失敗. 錯誤: %s";
   $lang['email_smtp_auth_pw'] = "鑑定用戶密碼失敗. 錯誤: %s";
   $lang['email_smtp_data_failure'] = "傳送資料失敗: %s";
   $lang['email_exit_status'] = "離開狀況編碼: %s";
   
  ?>