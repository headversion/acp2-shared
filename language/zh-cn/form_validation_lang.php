<?php

$lang['required'] = "%s 栏位是必须的";
$lang['isset'] = "%s 栏位不可为空。";
$lang['valid_email'] = "%s 栏位必须包含一个有效的电子邮件地址。";
$lang['valid_emails'] = "%s 栏位必须包含所有有效的电子邮件地址。";
$lang['valid_url'] = "%s 栏位必须包含一个有效的URL.";
$lang['valid_ip'] = "%s 栏位必须包含一个有效的IP地址.";
$lang['min_length'] = "%s 栏位的内容必须至少为%s 个字。";
$lang['max_length'] = "%s 栏位的内容不能超过%s 个字。";
$lang['exact_length'] = "%s 栏位的内容必须是长度正好%s 个字。";
$lang['alpha'] = "%s 栏位只能包含英文字母。";
$lang['alpha_numeric'] = "%s 栏位只能包含英文字母或数字。";
$lang['alpha_dash'] = "%s 栏位只能包含字母数字，下划线和破折号。";
$lang['numeric'] = "%s 栏位只能包含数字。";
$lang['is_numeric'] = "%s 栏位只能包含数字。";
$lang['integer'] = "%s 栏位必须包含一个整数。";
$lang['regex_match'] = "%s 栏位是不正确的格式。";
$lang['matches'] = "%s 栏位与%s 栏位并不匹配。";
$lang['is_natural'] = "%s 栏位只能包含正数。";
$lang['is_natural_no_zero'] = "%s 栏位必须是一个大于零的数字。";
$lang['decimal'] = "%s 栏位必须是一个十进制数。";
$lang['less_than'] = "%s 栏位必须是一个数字小于%s 的数字.";
$lang['greater_than'] = "%s 栏位必须是一个大于%s 的数字.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */