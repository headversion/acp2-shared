<?php

$lang['auth_required']           = 'You are trying to access protected area, please sign-in with your Logid ID and password first.';
$lang['auth_loginid']            = 'Login ID';
$lang['auth_password']           = 'Password';
$lang['auth_loginid_notfound']   = 'Login ID not found. Please try again.';
$lang['auth_password_incorrect'] = 'Password incorrect. Please try again.';
$lang['auth_unknown_error']      = 'Unknown error. Please contact your technical support to report this suitation.';

$lang['profile_password_change']                = 'Password Change';
$lang['profile_password_change_success']        = 'Password Change Successfully';
$lang['profile_password_change_fail']           = 'Sorry, We cannot change your password';
$lang['profile_password_old']                   = 'Current Password';
$lang['profile_password_current_description']   = 'Enter your valid password before change to new password';
$lang['profile_password_old_incorrect']         = 'Incorrect Password';
$lang['profile_password_new']                   = 'New Password';
$lang['profile_password_description']           = 'Enter your new password here. At least 6 characters length, letter words and numeric are accepted ';
$lang['profile_password_error_invalid']         = 'Invalid format of password';
$lang['profile_password_error_same']            = 'You cannot assign same password as your new password';
$lang['profile_password_retype']                = 'Re-type Password';
$lang['profile_password_retype_description']    = 'Enter your new password again';
$lang['profile_password_retype_error_notmatch'] = 'Your new password does not matched. Try again';