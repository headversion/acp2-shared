<?php

$lang['platform_name'] = 'ACP';

$lang['country_global'] = 'Global';
$lang['country_tw']     = 'Taiwan';
$lang['country_hk']     = 'Hong Kong';
$lang['country_cn']     = 'China';

$lang['select']             = 'Select';
$lang['error']              = 'Error';
$lang['users']              = 'Users';
$lang['administrators']     = 'Administrators';
$lang['members']            = 'Members';
$lang['static_content']     = 'Static Content';
$lang['resources']          = 'Resources';
$lang['files']              = 'Files';
$lang['reports']            = 'Reports';
$lang['tools']              = 'Tools';
$lang['detail']             = 'Detail';
$lang['details']            = 'Details';
$lang['actions']            = 'Actions';
$lang['remove']             = 'Remove';
$lang['counter']            = 'Counter';
$lang['language']           = 'Language';
$lang['copy_from']          = 'Copy From';
$lang['login']              = 'Login';
$lang['logout']             = 'Logout';
$lang['logout_message']     = 'You are logged out.';
$lang['backup_restore']     = 'Backup & Restore';
$lang['create']             = 'Create';
$lang['last_update']        = 'Last Updated';
$lang['keyword']            = 'Keyword';
$lang['options']            = 'Options';
$lang['basic']              = 'Basic';
$lang['media']              = 'Media';
$lang['meta']               = 'Meta';
$lang['add']                = 'Add';
$lang['edit']               = 'Edit';
$lang['delete']             = 'Delete';
$lang['display_order']      = 'Display Order';
$lang['save_display_order'] = 'Save Display Order';
$lang['range_from']         = 'From';
$lang['range_to']           = 'To';
$lang['change']             = 'Change';
$lang['save_changes']       = 'Save Changes';
$lang['discard_changes']    = 'Discard Changes';
$lang['all']                = 'All';
$lang['none']               = 'None';
$lang['not_available']      = 'N/A';
$lang['not_selected']       = 'Not selected';
$lang['if_supported']       = '(If supported)';
$lang['browser_filename']   = 'Filename';
$lang['browser_path']       = 'Path';
$lang['bulk_upload']        = 'Bulk Upload';
$lang['album_name']         = 'Album Name';

$lang['dashboard_heading']        = 'Dashboard';
$lang['preference_heading']       = 'Preference';
$lang['promotion_heading']        = 'Promotion';
$lang['banner_heading']           = 'Banner';
$lang['page_heading']             = 'Pages';
$lang['store_heading']            = 'Store';
$lang['article_heading']          = 'Article';
$lang['member_heading']           = 'Member';
$lang['client_heading']           = 'Client';
$lang['content_heading']          = 'Content';
$lang['region_heading']           = 'Region';
$lang['albums_heading']           = 'Albums';
$lang['photo_heading']            = 'Photos';
$lang['images_heading']           = 'Images';
$lang['project_heading']          = 'Projects';
$lang['product_heading']          = 'Products';
$lang['event_heading']            = 'Events';
$lang['brand_heading']            = 'Brands';
$lang['news_heading']             = 'News';
$lang['menu_heading']             = 'Menu';
$lang['images_manage']            = 'Images Manage';
$lang['misc_heading']             = 'Misc / Other';
$lang['album_heading']            = 'Album';
$lang['file_heading']             = 'File';
$lang['tag_heading']              = 'Tag';
$lang['file_heading']             = 'File';
$lang['user_heading']             = 'User';
$lang['member_heading']           = 'Member';
$lang['admin_heading']            = 'Administrator';
$lang['account_heading']          = 'Account';
$lang['role_heading']             = 'Role';
$lang['permission_heading']       = 'Permission';
$lang['resource_heading']         = 'Resource';
$lang['dashboard_heading']        = 'Dashboard';
$lang['preference_heading']       = 'Preference';
$lang['customer_service_heading'] = 'Customer Service';
$lang['newsletter_heading']       = 'Newsletter';
$lang['enquiry_heading']          = 'Enquiry';
$lang['post_heading']             = 'Post';
$lang['blog_heading']             = 'Blog';
$lang['category_heading']         = 'Category';
$lang['staticpage_heading']       = $lang['static_page_heading']       = 'Static Page';

$lang['filedrop_description']           = 'You can drag your images into this area for uploading files!';
$lang['filedrop_browser_support']       = '(Supported browsers are: Google Chrome 6+, Firefox 3.6+, Safari 5+, InternetExplorer 10+)';
$lang['file_list_dragzone_description'] = 'You can also drag your files to this area for upload files.';
$lang['file_list_dropfile_description'] = 'Drop here to upload your files instantly!';

$lang['crop_setting']       = 'Crop Setting';
$lang['crop_dimension']     = 'Dimension';
$lang['crop_source_image']  = 'Source';
$lang['crop_preview_image'] = 'Preview';

$lang['cb_apply_selected']       = 'Apply Selected';
$lang['remove_message']          = 'Are you sure to remove selected item(s)?';
$lang['listing_selected_action'] = '<i class="fa fa-cog"></i>';

$lang['record_success']   = 'Record has been saved.';
$lang['record_saved']     = 'Record has been saved.';
$lang['record_published'] = 'Record has been published.';
$lang['record_submit_error'] = 'Sorry, we cannot handle your submission.\nPlease try again.';
$lang['record_empty_error'] = 'Sorry, you have to fill-in the blank for all required fields.';

$lang['tab_basic']     = 'Basic Info';
$lang['tab_photo']     = 'Photo';
$lang['tab_top_sales'] = 'Top Sales';
$lang['tab_featured']  = 'Featured';

$lang['imagemanager_upload']          = 'Upload';
$lang['imagemanager_upload_limit']    = '(Max:{limit})';
$lang['imagemanager_status']          = 'Status';
$lang['imagemanager_status_all']      = 'All';
$lang['imagemanager_status_error']    = 'Error';
$lang['imagemanager_status_progress'] = 'Progress';
$lang['imagemanager_uploaded']        = 'Uploaded Image';

$lang['filetype_all']      = 'All File Types';
$lang['filetype_image']    = 'Image';
$lang['filetype_document'] = 'Document';
$lang['filetype_zip']      = 'Zip';
$lang['filetype_audio']    = 'Audio';

$lang['selected_file']     = 'Selected File';
$lang['no_selected_file']  = 'No Selected File';
$lang['no_selected_video'] = 'No Selected Video';
$lang['selected_image']    = 'Selected Image';
$lang['no_selected_image'] = 'No Selected Image';
$lang['selected_album']    = 'Selected Album';
$lang['no_selected_album'] = 'No Selected Album';

$lang['button_select_server_file'] = 'Select Server File';
$lang['button_import_server_file'] = 'Import Server File';
$lang['button_import_image']       = 'Import Server Image';
$lang['button_import_server_image']= 'Import Server Image';
$lang['button_update']             = 'Update';
$lang['button_yes']                = 'Yes';
$lang['button_no']                 = 'No';
$lang['button_select']             = 'Select';
$lang['button_done']               = 'Done';
$lang['button_import']             = 'Import';
$lang['button_export']             = 'Export';
$lang['button_select_image']       = 'Select Image';
$lang['button_select_file']        = 'Select File';
$lang['button_select_video']       = 'Select Video';
$lang['button_upload']             = 'Upload';
$lang['button_edit']               = 'Edit';
$lang['button_cancel']             = 'Cancel';
$lang['button_confirm']            = 'Confirm';
$lang['button_remove']             = 'Remove';
$lang['button_create']             = 'Create';
$lang['button_catalog'] = 'Catalog';
$lang['button_search']             = 'Search';
$lang['button_preview']            = 'Preview';
$lang['button_save']               = 'Save';
$lang['button_discard_changes']    = 'Discard Changes';
$lang['button_manage_category']    = 'Manage Category';
$lang['button_manage_tag']         = 'Manage Tag';
$lang['button_close']              = 'Close';
$lang['button_view']               = 'View';
$lang['button_list']               = 'List';
$lang['button_status_read']        = 'Mask as read';
$lang['button_status_unread']      = 'Mask as unread';
$lang['button_reload']             = 'Reload';
$lang['button_add']                = 'Add';
$lang['button_detail']             = 'Detail';
$lang['button_delete']             = 'Delete';
$lang['button_status_enable']      = 'Mark as Enable';
$lang['button_status_disable']     = 'Mark as Disable';
$lang['button_status_draft']       = 'Mark as Disable';
$lang['button_status_publish']     = 'Mark as Enable';
$lang['button_status_staging']     = 'Mark as Protected';
$lang['button_publish']            = 'Publish';
$lang['button_live']               = 'Live View';

$lang['form_required_field']   = '<i>Fill-in all blank if marked *</i>';
$lang['available_after_saved'] = 'Available after record saved';
$lang['placeholder_optional']  = '(Optional)';

$lang['field_title']                    = 'Title';
$lang['field_title_description']        = 'Text content to present this record in this site, social media feed and search engine.';
$lang['field_subtitle']                 = 'Subtitle';
$lang['field_publish_date']             = 'Publish Date';
$lang['field_publish_date_description'] = 'Content will be displayed after this date.';
$lang['field_create_date']              = 'Create Date';
$lang['field_modify_date']              = 'Modify Date';
$lang['field_description']              = 'Description';
$lang['field_description_description']  = 'Text content for social media feed and part of searchable result for search engine.';
$lang['field_content']                  = 'Content';
$lang['field_status']                   = 'Status';
$lang['field_status_description']       = 'Control who can read:'
		.'<ul>'
		.'<li>Disabled: No one can view and search</li>'
		.'<li>Enabled: Anyone can view and search</li>'
		.'<li>Protected: Anyone can view and but cannot search</li>'
		.'<ul>'
		;
$lang['field_cover_id']                 = 'Cover';
$lang['field_cover_id_description']     = 'This image will be shown in listing view and social media feed when sharing.';
$lang['field_cover_description']        = $lang['field_cover_id_description'];
$lang['field_album_id']                 = 'Album';
$lang['field_album_id_description']     = 'List of selected images will be shown as slideshow.';
$lang['field_album_description']        = $lang['field_album_id_description'];
$lang['field_priority']                 = 'Priority';
$lang['field_priority_description']     = 'Lower value will be displayed first';
$lang['field_slug']                     = 'SEO URL Name';
$lang['field_slug_description']         = 'Used for search engine. Will use record id if leave it blank.<br />Accepted type of characters are: english letter, numeric, dot, dash and underscore.';
$lang['field_categories']               = 'Related Categories';
$lang['field_category']                 = 'Category';
$lang['field_main_category']            = 'Main Category';
$lang['field_tags']                     = 'Tags';
$lang['field_tag']                      = 'Tag';
$lang['field_name']                     = 'Name';
$lang['field_url']                      = 'URL';
$lang['field_link']                     = 'Link';
$lang['field_home_visible']             = 'Home Visible';
$lang['field_plain']                    = 'Plain';
$lang['field_subject']                  = 'Subject';
$lang['field_plain']                    = 'Plain Content';
$lang['field_is_published']             = 'Published';
$lang['field_parent_id']                = 'Parent';
$lang['field_file_name']				= 'File Name';
$lang['field_upload_date']				= 'Upload Date';
$lang['field_sys_name']                 = 'System Identifier';
$lang['field_start_date']               = 'Start Date';
$lang['field_start_date_description']   = 'Content will be appear after this date.';
$lang['field_end_date']                 = 'End Date';
$lang['field_end_date_description']     = 'Content will be disappear after this date.';
$lang['field_loc_active']               = 'Active for this language';
$lang['field_loc_active_description']   = 'Please select &quot;Yes&quot; if this language is enable for use.';

$lang['subscribe_0'] = 'No';
$lang['subscribe_1'] = 'Yes';

$lang['home_visible_0'] = 'No';
$lang['home_visible_1'] = 'Yes';

$lang['status_0'] = 'Disabled';
$lang['status_1'] = 'Enabled';
$lang['status_2'] = 'Protected';

$lang['is_pushed_0'] = 'Pending to live';
$lang['is_pushed_1'] = 'Published to live';
$lang['is_pushed_2'] = 'Updated after last published';

$lang['quota_type']             = 'Quota Type';
$lang['quota_type_description'] = 'For each member, limitation of performing this mission.';
$lang['quota_once']             = 'Play once';
$lang['quota_once_plan']        = 'Play once for each plan';
$lang['quota_unlimited']        = 'No limitation';

$lang['hints_changepassword'] = 'Click here to set your login password';

$lang['error_processing'] = 'Processing...';

