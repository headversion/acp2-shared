<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');  
/** 
 * LMS_Render library for CodeIgniter
 * @author      leman 
 * @copyright   Copyright (c) 2010, LMSWork. 
 * @license     http://codeigniter.com/user_guide/license.html 
 * @link        http://lmswork.com 
 * @since       Version 1.0 
 * @filesource 
 *  
 */  
  
// ------------------------------------------------------------------------  
if (!class_exists('JSMin', false)) {
class JSMin{const ORD_LF=10;const ORD_SPACE=32;const ACTION_KEEP_A=1;const ACTION_DELETE_A=2;const ACTION_DELETE_A_B=3;protected$a="\n";protected$b='';protected$input='';protected$inputIndex=0;protected$inputLength=0;protected$lookAhead=null;protected$output='';protected$lastByteOut='';static function minify($L){$J=new JSMin($L);return$J->min();}function __construct($K){$this->input=str_replace("\r\n","\n",$K);$this->inputLength=strlen($this->input);}protected function action($D){if($D===self::ACTION_DELETE_A_B&&$this->b===' '&&($this->a==='+'||$this->a==='-')){if($this->input[$this->inputIndex]===$this->a){$D=self::ACTION_KEEP_A;}}switch($D){case self::ACTION_KEEP_A:$this->output.=$this->a;$this->lastByteOut=$this->a;case self::ACTION_DELETE_A:$this->a=$this->b;if($this->a==="'"||$this->a==='"'){$G=$this->a;while(true){$this->output.=$this->a;$this->lastByteOut=$this->a;$this->a=$this->get();if($this->a===$this->b){break;}if(ord($this->a)<=self::ORD_LF){throw new JSMin_UnterminatedStringException('Unterminated string literal.'.$this->inputIndex.": {$G}");}$G.=$this->a;if($this->a==='\\'){$this->output.=$this->a;$this->lastByteOut=$this->a;$this->a=$this->get();$G.=$this->a;}}}case self::ACTION_DELETE_A_B:$this->b=$this->next();if($this->b==='/'&&$this->isRegexpLiteral()){$this->output.=$this->a.$this->b;$E='/';while(true){$this->a=$this->get();$E.=$this->a;if($this->a==='['){while(true){$this->output.=$this->a;$this->a=$this->get();if($this->a===']'){break;}elseif($this->a==='\\'){$this->output.=$this->a;$this->a=$this->get();$E.=$this->a;}elseif(ord($this->a)<=self::ORD_LF){throw new JSMin_UnterminatedRegExpException('Unterminated regular expression set in regex literal.'.$this->inputIndex.": {$E}");}}}elseif($this->a==='/'){break;}elseif($this->a==='\\'){$this->output.=$this->a;$this->a=$this->get();$E.=$this->a;}elseif(ord($this->a)<=self::ORD_LF){throw new JSMin_UnterminatedRegExpException('Unterminated regular expression literal.'.$this->inputIndex.": {$E}");}$this->output.=$this->a;$this->lastByteOut=$this->a;}$this->b=$this->next();}}}protected function isRegexpLiteral(){if(false!==strpos("\n{;(,=:[!&|?",$this->a)){return true;}if(' '===$this->a){$H=strlen($this->output);if($H<2){return true;}if(preg_match('/(?:case|else|in|return|typeof)$/',$this->output,$I)){if($this->output===$I[0]){return true;}$M=substr($this->output,$H-strlen($I[0])-1,1);if(!$this->isAlphaNum($M)){return true;}}}return false;}protected function get(){$C=$this->lookAhead;$this->lookAhead=null;if($C===null){if($this->inputIndex<$this->inputLength){$C=$this->input[$this->inputIndex];$this->inputIndex+=1;}else{return null;}}if($C==="\r"||$C==="\n"){return"\n";}if(ord($C)<self::ORD_SPACE){return' ';}return$C;}protected function isAlphaNum($C){return(preg_match('/^[0-9a-zA-Z_\\$\\\\]$/',$C)||ord($C)>126);}protected function singleLineComment(){$B='';while(true){$A=$this->get();$B.=$A;if(ord($A)<=self::ORD_LF){if(preg_match('/^\\/@(?:cc_on|if|elif|else|end)\\b/',$B)){return"/{$B}";}return$A;}}}protected function multipleLineComment(){$this->get();$B='';while(true){$A=$this->get();if($A==='*'){if($this->peek()==='/'){$this->get();if(0===strpos($B,'!')){return"\n/*!".substr($B,1)."*/\n";}if(preg_match('/^@(?:cc_on|if|elif|else|end)\\b/',$B)){return"/*{$B}*/";}return' ';}}elseif($A===null){throw new JSMin_UnterminatedCommentException("JSMin: Unterminated comment at byte ".$this->inputIndex.": /*{$B}");}$B.=$A;}}protected function min(){if($this->output!==''){return$this->output;}if(0==strncmp($this->peek(),"\xef",1)){$this->get();$this->get();$this->get();}$F=null;if(function_exists('mb_strlen')&&((int)ini_get('mbstring.func_overload')&2)){$F=mb_internal_encoding();mb_internal_encoding('8bit');}$this->input=str_replace("\r\n","\n",$this->input);$this->inputLength=strlen($this->input);$this->action(self::ACTION_DELETE_A_B);while($this->a!==null){$D=self::ACTION_KEEP_A;if($this->a===' '){if(($this->lastByteOut==='+'||$this->lastByteOut==='-')&&($this->b===$this->lastByteOut)){}elseif(!$this->isAlphaNum($this->b)){$D=self::ACTION_DELETE_A;}}elseif($this->a==="\n"){if($this->b===' '){$D=self::ACTION_DELETE_A_B;}elseif($this->b===null||(false===strpos('{[(+-!~',$this->b)&&!$this->isAlphaNum($this->b))){$D=self::ACTION_DELETE_A;}}elseif(!$this->isAlphaNum($this->a)){if($this->b===' '||($this->b==="\n"&&(false===strpos('}])+-"\'',$this->a)))){$D=self::ACTION_DELETE_A_B;}}$this->action($D);}$this->output=trim($this->output);if($F!==null){mb_internal_encoding($F);}return$this->output;}protected function next(){$A=$this->get();if($A!=='/'){return$A;}switch($this->peek()){case'/':return$this->singleLineComment();case'*':return$this->multipleLineComment();default:return$A;}}protected function peek(){$this->lookAhead=$this->get();return$this->lookAhead;}}class JSMin_UnterminatedStringException extends Exception{}class JSMin_UnterminatedCommentException extends Exception{}class JSMin_UnterminatedRegExpException extends Exception{}
}

class ElementHelper {
	
	var $_assigned_paths = array();
	var $_meta = array();
	var $_links = array();
	
	function __construct(){}

	public function print_res($type='head'){

		foreach($this->_links as $idx =>$row)
		{
			if(empty($row['attrs'])) continue;
			$row_type = !isset($row['position']) ? 'head' : $row['position'];
			if($row_type == $type)
				print $this->_link_tag($row['attrs'], !empty($row['options']) ? $row['options'] : NULL)."\n";
		}
	}
	
	public function printInline($pos='head')
	{
		return $this->print_res($pos);
	}
	
	public function link($path,$attrs=false,$pos='head',$options=false){

		if(isset($this->_assigned_paths[$path])){
			return;
		}
		$this->_assigned_paths[$path] = true;
		
		$attrs['href'] = $path;
		if($pos == 'inline') print $this->_link_tag($attrs,$options);
		else $this->_links[] = array('attrs'=>$attrs, 'position'=>$pos,'options'=>$options);  
	}
	
	function _attr_escape($str)
	{
		return html_attribute_escape($str);
	}
	
	function _array_to_attr_str($attrs)
	{
		return array_to_html_attribute($attrs);
	}
	
	function _link_tag($attrs=false,$options=false){
		return $this->_handle_options('<link'.$this->_array_to_attr_str($attrs).'/>',$options);
	}
	
	
	function _tag($path=false,$name=false,$attrs=false,$options=false){
		if(!$name ) $name = $path;
		if(!$attrs) $attrs = array();
		$attrs['href'] = $path;
		return $this->_handle_options('<a'.$this->_array_to_attr_str($attrs).'>'.$name.'</a>', $options);
	}

	function _handle_options($code='', $options=false){

		$str = '';
		if(is_array($options) && !empty($options['pre_code']))
			$str.= $options['pre_code'];
		$str.= $code;
		if(is_array($options) && !empty($options['post_code']))
			$str.= $options['post_code'];
		return $str;
	}
}

class Render extends ElementHelper
{
	
	var $className = array();
	
	var $_meta_property = array();
	
	var $_meta = array();
	
	var $_css = array();
	
	var $_scripts = array();
	
	var $_linked_paths = array();

	var $compress_js = true;
	
	function __construct(){
		parent::__construct();
		log_message('debug','Render Class Initialized');
	}

	/*****/
	// Meta Tags


	public function reset_meta_property($name, $content = ''){
		$this->_meta_property[$name] = NULL;
	}

	public function set_meta_property($name, $content = ''){
		$this->_meta_property[$name] = $content;
	}

	public function add_meta_property($name, $content = ''){
		$raw = NULL;
		if(isset($this->_meta_property[$name])){
			$raw = $this->_meta_property[$name];
			if(!is_array($raw)){
				$this->_meta_property[$name] = array( $raw );
			}
		}else{
			$this->_meta_property[$name] = array();
		}

		$this->_meta_property[$name][] = $content;
	}

	/*****/
	// CSS
	public function getClassName($key)
	{
		if(!isset($this->className[$key])) return '';
		return implode(" ",$this->className[$key]);
	}
	
	public function addClass($key,$val)
	{
		$ary = explode(" ",$val);
		
		if(!isset($this->className[$key])) $this->className[$key] = array();
		
		foreach($ary as $idx=>$val){
			if(!in_array($val,$this->className[$key])){
				$this->className[$key][] = $val;
			}
		}
		return $this;
	}
	
	public function removeClass($key,$val)
	{
		if(!$this->className[$key]) $this->className[$key] = array();
		$ary = explode(" ",$val);
		
		foreach($ary as $idx=>$val){
			if(in_array($val,$this->className[$key])){
				unset($this->className[$key]);
			}
		}
		return $this;
	}
	
	public function css($path,$attrs=false,$pos='head', $options=false){
		return $this->css_import($path, $attrs, $pos,$options);
	}
	
	public function css_import($path,$attrs=false,$pos='head', $options=false){


		if(isset($this->_assigned_paths[$path])){
			return;
		}
		$this->_assigned_paths[$path] = true;

		if(!$attrs) $attrs = array();
		$attrs['href'] = $path;
		if(!isset($attrs['rel']))$attrs['rel'] = 'stylesheet';
		if(!isset($attrs['type']))$attrs['type'] = 'text/css';
		//$attrs['src'] = $path;
		if($pos == 'inline'){
			print $this->_link_tag($attrs,$options);
		}else{
			$this->_links[] = array('attrs'=>$attrs,'position'=>$pos,'options'=>$options);
		}

		return TRUE;
	}

	
	public function css_embed($view, $vals=NULL,$attrs=false,$pos='head', $options=false){
		if(!$attrs) $attrs = array();
		$attrs['href'] = $path;
		if(!isset($attrs['rel']))$attrs['rel'] = 'stylesheet';
		if(!isset($attrs['type']))$attrs['type'] = 'text/css';
		$CI = &get_instance();
		$code = $CI -> load -> view($view, $vals, TRUE);
		if($pos == 'inline'){
			print $this->_css_tag($attrs,$code,$options);
			return;
		}else{
			$this->_css[] = array('attrs'=>$attrs,'code'=>$code,'position'=>$pos,'options'=>$options);
		}
	}
	
	/*****/
	// JS
	public function js_minify($code=''){
		return JSMin::minify($code);
	}

	public function js($code='',$attrs=false,$pos='head',$options=false){
		$merged = $code;

		if(!empty($merged) && $this->compress_js){
			$merged = $this->js_minify($code);
		}
		if($pos == 'inline'){
			print $this->_script_tag($attrs,$merged,$options);
		}else{
			$this->_scripts[] = array('attrs'=>$attrs,'code'=>$merged,'position'=>$pos,'options'=>$options);
		}
		return TRUE;
	}

	public function js_embed($src, $vals=false, $attrs=false, $pos = 'head',$options=false){
		$CI = &get_instance();
		$code = $CI -> load -> view($src, $vals, TRUE);
		$merged = $code;

		if(empty($code) && empty($attrs)) {
			log_message('debug','Render/js_embed, No code/attributes have been assigned.');
			return;
		}

		if($this->compress_js){
			$merged = $this->js_minify($code);
			if($this->_is_debug() ){
				$merged = "\r\n".'/*@src:'.$src."*/\r\n".$merged;
			}
		}
		if($pos == 'inline'){
			print $this->_script_tag($attrs,$merged,$options);
		}else{
			$this->_scripts[] = array('attrs'=>$attrs,'code'=>$merged,'position'=>$pos,'options'=>$options);
		}
	}
	
	public function js_import($path,$extra_attr=false,$pos='head',$options=false)
	{
		if(isset($this->_assigned_paths[$path])){
			return;
		}
		$this->_assigned_paths[$path] = true;

		$attrs = array('type'=>'text/javascript','src'=>$path);
		if(!empty($extra_attr) && is_array($extra_attr)){
			$attrs = array_merge($attrs,$extra_attr );
		}
		
		if($pos == 'inline'){
			print $this->_script_tag($attrs,'',$options);
		}
		else{
			$this->_scripts[] = array('attrs'=>$attrs, 'position'=>$pos,'options'=>$options);
		}
		return TRUE;
	}
	
	public function meta($attrs=false)
	{
		$_hash = md5(isset($attrs['name']) ? $attrs['name'] : json_encode($attrs));

		$this->_meta [ $_hash  ] = array('attrs'=>$attrs,'position'=>'head');
	}
	
	public function set_meta_content($name='',$content='',$pos='head')
	{
		$attrs = array();
		$attrs['name'] = $name;
		$attrs['content'] = $content;
		$_hash = md5(isset($attrs['name']) ? $attrs['name'] : json_encode($attrs));
		$this->_meta [$_hash] = array('attrs'=>$attrs,'position'=>$pos);
	}

	public function add_meta_content($name, $content = '',$pos='head'){

		$attrs = array();
		$attrs['name'] = $name;
		$attrs['content'] = $content;
		$_hash = md5(isset($attrs['name']) ? $attrs['name'] : json_encode($attrs));

		$raw = NULL;
		if(isset($this->_meta[$_hash])){
			$raw = $this->_meta[$_hash];
			if(!is_array($raw)){
				$this->_meta[$_hash] = array( $raw );
			}
		}else{
			$this->_meta[$_hash] = array();
		}

		$this->_meta[$_hash][] = array('attrs'=>$attrs,'position'=>$pos);
	}
	
	public function print_res($type = 'head')
	{
		$counter = 0;
		foreach($this->_meta as $idx =>$row)
		{
			$row_type = !isset($row['position']) || !$row['position'] ? 'head' : $row['position'];
			if($row_type == $type){
				if($counter < 1 && $this->_is_debug()){
					print "<!-- Meta -->\r\n";
				}
				$counter++;

				// print multiple meta content
				if(!empty($row['attrs'])){
					print $this->_meta_tag($row['attrs'])."\r\n";
				}elseif(is_array($row)){
					foreach($row as $_row)
						print $this->_meta_tag($_row['attrs'])."\r\n";
				}
			}
		}


		if($type == 'head'){
			$counter = 0;
			foreach($this->_meta_property as $name => $content)
			{
				if($counter < 1 && $this->_is_debug()){
					print "<!-- Meta Property -->\r\n";
				}
				$counter++;
				if(is_array($content)){
					foreach($content as $_content)
						print $this->_meta_tag(array('property'=>$name,'content'=>$_content))."\r\n";
				}elseif($content !== NULL){
					print $this->_meta_tag(array('property'=>$name,'content'=>$content))."\r\n";
				}
			
			}
		}

		parent::print_res($type);

		$counter = 0;
		foreach($this->_css as $idx =>$row)
		{
			$row_type = !isset($row['position']) || !$row['position'] ? 'head' : $row['position'];
			if($row_type == $type){
				if($counter < 1 && $this->_is_debug()){
					print "<!-- Stylesheet -->\r\n";
				}
				$counter++;
				print $this->_css_tag($row['attrs'],isset($row['code']) ? $row['code'] : '', !empty($row['options']) ? $row['options'] : NULL)."\r\n";
			}
		}
		$counter = 0;
		foreach($this->_scripts as $idx =>$row)
		{
			$row_type = !isset($row['position']) || !$row['position'] ? 'head' : $row['position'];
			if($row_type == $type){
				if($counter < 1 && $this->_is_debug()){
					print "<!-- JS -->\r\n";
				}
				$counter++;
				print $this->_script_tag($row['attrs'],isset($row['code']) ? $row['code'] : '', !empty($row['options']) ? $row['options'] : NULL)."\r\n";
			}
		}
	}
	
	function _code_tag($code=false,$attrs=false,$options=false)
	{
		if(!$code || empty($code)) return '';
		if(!$attrs) $attrs = array();
		if(!isset($attrs['type'])) $attrs['type'] = 'text/javascript';

		return $this->_handle_options('<script'.$this->_array_to_attr_str($attrs).'>'.$code.'</script>',$options);
	}

	function _meta_tag($attrs,$options=false)
	{
		if(!$attrs) $attrs = array();
		$str = '';
		return $this->_handle_options('<meta'.$this->_array_to_attr_str($attrs).' />',$options);
	}
	
	
	function _css_tag($attrs=false,$code='',$options=false)
	{
		if(!$attrs) $attrs = array();
		$str = '';

		if(!isset($attrs['type'])) $attrs['type'] = 'text/stylesheet';
		return $this->_handle_options('<style'.$this->_array_to_attr_str($attrs).'>'.$code.'</style>',$options);
	}
	
	function _script_tag($attrs=false,$code='', $options=false)
	{
		if(!$attrs) $attrs = array();
		if(!isset($attrs['type'])) $attrs['type'] = 'text/javascript';

		return $this->_handle_options('<script'.$this->_array_to_attr_str($attrs).'>'.$code.'</script>',$options);
	}

	function _is_debug(){
		return defined('ENVIRONMENT') && !in_array(ENVIRONMENT, array('staging','production'));
	}


}