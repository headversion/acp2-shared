<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class LMS_Auth{
	var $table = 'users';
	var $field = array(
		'uid'=>'id',
		'loginkey'=>'username',
		'password'=>'password',
		);
	var $path = array(
		'login'=>'auth/signin'
		);
	var $views = array(
		'restrict'=>'auth/restrict'
		);
	var $default_params = array('required'=>'true');
	var $encrypt_config_key = NULL;
	var $_user = NULL;
	var $CI = NULL;
	var $session = array(
		'id'=>'auth_id',
		'before_login'=>'before_login',
		);
		
	function LMS_Auth($config=false){
		
		if(isset($config['encrypt_config_key'])){
			$this->encrypt_config_key = $config['encrypt_config_key'];
		}
		
		if(isset($config['table']))
			$this->table = $config['table'];
		
		if(isset($config['field']))
			$this->field = array_merge($this->field,$config['field']);
		
		if(isset($config['path']))
			$this->path = array_merge($this->path,$config['path']);
			
		if(isset($config['views']))
			$this->views = array_merge($this->views,$config['views']);
			
		if(isset($config['default_params']))
			$this->default_params = array_merge($this->default_params,$config['default_params']);
			
		if(isset($config['activiate']))
			$this->activiate($config['activiate']);
	}
	
	function activiate($key='lms'){
		
		
		if(!$this->CI){
			$this->CI =& get_instance();
			if(!isset($this->CI->session)){
				$this->CI->load->library('session');
			}
			if(!isset($this->CI->encrypt)){
				$this->CI->load->library('encrypt');
			}

			$this->CI->load->helper('url');
			$this->CI->load->database();
		}
		
		$this->_session_keys['id'] = $key.'_id';
		
		$_userid = $this->get_id();
		
		// erase previous data
		$this->_user = NULL;
		if($_userid)
			$this->_user = $this->get_user_by_id($_userid);
	}
	
	function set_id($val){
		return $this->CI->session->set_userdata($this->_session_keys['id'],$val);
	}
	
	function get_id(){
		return $this->CI->session->userdata($this->_session_keys['id']);
	}
	
	function deactiviate()
	{
		$this->_user = NULL;
		$this->CI = NULL;
	}
	
	function encrypt($text= ''){
		$val = NULL;
		if(!empty($this->encrypt_config_key)){
			$val = $this->CI->encrypt->encode($text,$this->CI->config->item($this->encrypt_config_key));
		}else{
			$val = $this->CI->encrypt->encode($text);
		}
		
		//log_message('debug','LMS_Auth/encrypt, text='.$text.', key='.$this->CI->config->item($this->encrypt_config_key).', val='.$val);
		return $val;
	}
	
	function decrypt($text= ''){
		$val = NULL;
		if(!empty($this->encrypt_config_key)){
			$val = $this->CI->encrypt->decode($text,$this->CI->config->item($this->encrypt_config_key));
		}else{
			$val = $this->CI->encrypt->decode($text);
		}
		
		//log_message('debug','LMS_Auth/decrypt, text='.$text.', key='.$this->CI->config->item($this->encrypt_config_key).', val='.$val);
		return $val;
	}
	
	function restrict(){
		if(!$this->is_login()){
			
			$forward = site_url($this->CI->uri->uri_string);
			
			if(isset($_SERVER['QUERY_STRING'])){
				if(strlen($_SERVER['QUERY_STRING'])>0)
					$forward.= '?'.$_SERVER['QUERY_STRING'];
			}
				
			$required_params = array('forward'=>rawurlencode($forward));
			$params = array_merge($this->default_params,$required_params);
			
			$url = $this->path['login'];
			if($params && count($params)>0) $url.='?'.http_build_query($params);
			
			// modified on 4 DEC 2011
			// by leman
			
			if($this->CI->uri->extension() != ''){
				$this->CI->load->view($this->views['restrict'].'.'.$this->CI->uri->extension().EXT,array('login_uri'=>$url));
				return true;
			}
			
			// if other request, redirect to login page
			//redirect($url);
			$this->CI->load->view($this->views['restrict'],array('login_uri'=>$url));
			return true;
		}
		return false;
	}
	
	function is_login(){
		
		$id = $this->get_id();
		if(!$id || empty($id))
			return false;
		if(!$this->_user || $this->_user[$this->field['uid']] != $id)
			$this->_user = $this->get_user_by_id($id);
		if(!$this->_user || !isset($this->_user[$this->field['uid']]))
			return false;
		return true;
	}
	
	
	function check_password($row,$pass){
		$decrypted_str = $this->decrypt($row[$this->field['password']]);
		return $decrypted_str == $pass;
	}
	
	function check_access($params=false,$pass=''){
		
		$row = $this->get_user($params);
		$validPass = $this->check_password($row,$pass);
		if(!isset($row) || empty($row) || empty($pass)){
			return 2;
		}elseif(!$validPass){
			return 3;
		}elseif($validPass){
			return 1;
		}
		return -1;
	}
	
	function login($params)
	{
		$user = $this->get_user($params);
		
		if(isset($user[$this->field['uid']])){
		
			$this->_user = $user; 	
			$this->_after_login();
			
			return TRUE;	
		}
		return FALSE;
	}
	
	function logout(){
		$this->CI->session->unset_userdata($this->_session_keys['id']);
		
		$this->_user = NULL;
		return TRUE;
	}
	
	function _after_login()
	{
		$this->set_id($this->_user[$this->field['uid']]);
	}
	
	function get_user($params){
		if(!$this->CI) return NULL;
		
		if(!is_array($params)){
			if(is_array($this->field['loginkey'])){
				foreach ($this->field['loginkey'] as $key => $field_name) {
					
					$this->CI->db->where_or($field_name, $params);
				}
			}else{
				$this->CI->db->where($this->field['loginkey'], $params);
			}
		}else{
			$this->CI->db->where($params);
		}
		
		
		// we turn off cache always
		$this->CI->db->cache_off();
		$query = $this->CI->db->get($this->table);

		if(!$query)return NULL;
		if($query->num_rows() == 1){
			$row = $query->row_array();
			return $row;
		}
		return NULL;
	}
	
	function get_user_by_id($id){
		if(!$this->CI) return NULL;
		
		$this->CI->db->where($this->field['uid'],$id);
		$this->CI->db->cache_off();
		$query = $this->CI->db->get($this->table);

		if(!$query)return NULL;
		if($query->num_rows() == 1){
			return $query->row_array();
		}
		return NULL;
	}
}
?>