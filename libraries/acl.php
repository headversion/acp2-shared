<?php

class Acl
{
	var $perms = array();		//Array : Stores the permissions for the user
	var $user_id;			//Integer : Stores the ID of the current user
	var $user_roles = array();	//Array : Stores the roles of the current user
	var $user_role_ids = array();
	var $ci;
	
	var $tables_keys = array('users','roles','permissions','users_roles','uers_permissions','roles_permissions');
	var $fields = array(
		'user_id' => 'user_id',
		'permission_key'=>'key',
	);
	var $tables = array();
	/*
	var $tables = array(
		'users'=>'users',
		'roles'=>'roles',
		'users_roles'=>'users_roles',
		'users_permissions'=>'users_permissions',
		'permissions'=>'permissions',
		'roles_permissions'=>'roles_permissions',
	);
	*/
	function __construct($config=array()) {
		$this->ci = &get_instance();
		$this->load = $this->ci->load;
		
		if(isset($config['tables'])){

			if(is_array($config['tables'])){
				foreach($config['tables'] as $key => $name){
					$this->tables[ $key] = $name;
				}
			}
		}
		
		if(isset($config['fields'])){
			if(is_array($config['fields'])){
				foreach($config['fields'] as $key => $name){
					$this->fields[ $key] = $name;
				}
			}
		}
		if(isset( $config['user_id']) && !empty($config['user_id']))
		{
			$this->user_id = $config['user_id'];
		}
		
		log_message('debug','ACL Class initialized.');
		
		if(isset( $config['user_id']) && !empty($config['user_id']))
		{
			$this->set_user_id( $config['user_id']);
		}
	}
	
	function set_user_id($new_user_id){
		$this->user_id = $new_user_id;
		
		$this->ci->load->helper('cache');
		$this->user_roles = cache_get('acl/'.$new_user_id.'/roles');
		$this->user_role_ids = cache_get('acl/'.$new_user_id.'/roles');
		
		if(empty($this->user_roles))
			$this->user_roles = $this->get_user_roles();
		if(empty($this->user_role_ids))
			$this->user_role_ids = $this->get_user_role_ids();
		$this->rebuild();
	}

	function rebuild() {
		$this->perms = array();
		//first, get the rules for the user's role
		if (count($this->user_roles) > 0)
		{
			$this->perms = array_merge($this->perms,$this->get_role_permissions($this->user_role_ids));
		}
		//then, get the individual user permissions
		$this->perms = array_merge($this->perms,$this->get_user_permissions($this->user_id));
	}

	function get_detail_from_permid($permission_id) {
		$this->ci->db->select('id,name,key');
		$this->ci->db->where('id',$permission_id);
		$sql = $this->ci->db->get($this->table('permissions'),1);
		$data = $sql->row_array();
		
		return $data;
	}
	
	function get_permkey_from_id($permission_id){
		
		$key_field = $this->field('permission_key');
		$this->ci->db->select($key_field);
		$this->ci->db->where('id',$permission_id);
		$sql = $this->ci->db->get($this->table('permissions'));
		$data = $sql->row_array();
		
		if(isset($data['key']))
			return $data['key'];
		return null;
	}

	function get_rolename_from_roleid($role_id) {
		$this->ci->db->select('name');
		$this->ci->db->where('id',$role_id);
		$sql = $this->ci->db->get($this->table('roles'));
		$data = $sql->row_array();
		
		if(isset($data['name']))
			return $data['name'];
		return null;
	}

	function get_user_roles() {
		//$strSQL = "SELECT * FROM `".DB_PREFIX."user_roles` WHERE `userID` = " . $this->user_id . " ORDER BY `created` ASC";

		$this->ci->db->where(array(
			$this->field('user_id') => $this->user_id
		));
		
		$now = date('Y-m-d H:i:s');
		
		if($this->field('user_id')!=NULL){
			$this->ci->db->where($this->field('user_id') , $this->user_id);
		}
		
		if($this->field('start_date')!=NULL){
			$this->ci->db->where('('.$this->field('start_date').' <='.$this->ci->db->escape($now).' OR '.$this->field('start_date') .' IS NULL)');
		}
		
		if($this->field('end_date')!=NULL){
			$this->ci->db->where('('.$this->field('end_date').' >'.$this->ci->db->escape($now).' OR '.$this->field('end_date') .' IS NULL)');
		}
		if($this->field('is_active')!=NULL){
			$this->ci->db->where($this->field('is_active'),'1');
		}
		
		$this->ci->db->order_by( $this->field('created') ,'asc');
		$query = $this->ci->db->get($this->table('users_roles'));
		$data = $query->result_array();

		$resp = array();
		foreach( $data as $row )
		{
			$resp[] = $row;
		}
		return $resp;
	}

	function get_user_role_ids() {
		//$strSQL = "SELECT * FROM `".DB_PREFIX."user_roles` WHERE `userID` = " . $this->user_id . " ORDER BY `created` ASC";

		$this->ci->db->where(array(
			$this->field('user_id') => $this->user_id
		));
		$this->ci->db->order_by( $this->field('created') ,'asc');
		$query = $this->ci->db->get($this->table('users_roles'));
		$data = $query->result_array();

		$resp = array();
		foreach( $data as $row )
		{
			$resp[] = $row[ $this->field('role_id')  ];
		}
		return $resp;
	}

	function get_all_roles($format='ids') {
		$format = strtolower($format);
		//$strSQL = "SELECT * FROM `".DB_PREFIX."roles` ORDER BY `roleName` ASC";
		$this->ci->db->order_by('name','asc');
		$sql = $this->ci->db->get($this->table('roles'));
		$data = $sql->result_array();

		$resp = array();
		foreach( $data as $row )
		{
			if ($format == 'full')
			{
				$resp[] = array('id' => $row['id'], 'name' => $row['name'], );
			} else {
				$resp[] = $row['id'];
			}
		}
		return $resp;
	}

	function get_all_permissions($format='ids') {
		$format = strtolower($format);
		//$strSQL = "SELECT * FROM `".DB_PREFIX."permissions` ORDER BY `permKey` ASC";

		$this->ci->db->order_by($this->field('key'),'asc');
		$sql = $this->ci->db->get($this->table('permissions'));
		$data = $sql->result_array();

		$resp = array();
		foreach( $data as $row )
		{
			if ($format == 'full')
			{
				$resp[$row['key']] = array('id' => $row['id'], 'name' => $row['name'], 'key' => $row['key']);
			} else {
				$resp[] = $row['id'];
			}
		}
		return $resp;
	}

	function get_role_permissions($role) {
		
		$role_id_field = $this->field('role_id');
		if (is_array($role))
		{
			//$roleSQL = "SELECT * FROM `".DB_PREFIX."role_perms` WHERE `roleID` IN (" . implode(",",$role) . ") ORDER BY `ID` ASC";
			$this->ci->db->where_in($role_id_field,$role);
		} else {
			//$roleSQL = "SELECT * FROM `".DB_PREFIX."role_perms` WHERE `roleID` = " . floatval($role) . " ORDER BY `ID` ASC";
			$this->ci->db->where($role_id_field,$role);

		}
		
		$perm_id_field = $this->field('permission_id');
		$perm_key_field = $this->field('permission_key');
		$perm_name_field = $this->field('name');
		
		$this->ci->db->order_by('id','asc');
		$sql = $this->ci->db->get($this->table('roles_permissions')); //$this->db->select($roleSQL);
		$data = $sql->result_array();
		$perms = array();
		foreach( $data as $row )
		{
			$perm_key = $this->get_permkey_from_id($row[$perm_id_field]);
			if(empty($perm_key))
				continue;
			$perm_row = $this->get_detail_from_permid($row[$perm_id_field]);
			$perm_name = $perm_row[$perm_name_field];
			
			if ($row['value'] == '1') {
				$has_permission = true;
			} else {
				$has_permission = false;
			}
			
			$perms[$perm_key] = array(
				'perm' => $perm_key,
				'inheritted' => true,
				'value' => $has_permission,
				'name' =>$perm_name,
				'id' => $row[$perm_id_field]
				);
		}
		return $perms;
	}

	function get_user_permissions($user_id) {
		//$strSQL = "SELECT * FROM `".DB_PREFIX."user_perms` WHERE `userID` = " . floatval($user_id) . " ORDER BY `created` ASC";
		
		$perm_id_field = $this->field('permission_id');
		$perm_key_field = $this->field('permission_key');
		$perm_name_field = $this->field('name');

		$this->ci->db->where($this->field('user_id'),($user_id));
		$this->ci->db->order_by($this->field('created'),'asc');
		$sql = $this->ci->db->get($this->table('users_permissions'));
		$data = $sql->result_array();

		$perms = array();
		foreach( $data as $row )
		{
			$perm_key = $this->get_permkey_from_id($row[$perm_id_field]);
			if(empty($perm_key))
				continue;
			$perm_row = $this->get_detail_from_permid($row[$perm_id_field]);
			$perm_name = $perm_row[$perm_name_field];
			
			if ($row['value'] == '1') {
				$has_permission = true;
			} else {
				$has_permission = false;
			}
			$perms[$perm_key] = array(
				'perm' => $perm_key,
				'inheritted' => false,
				'value' => $has_permission,
				'name' => $perm_name,
				'id' => $row[$perm_id_field]
				);
		}
		return $perms;
	}

	function has_role($role_id) {
		foreach($this->user_roles as $k => $v)
		{
			if ($v === $role_id)
			{
				return true;
			}
		}
		return false;
	}

	function has_permission($scope) {
		
		if(is_array($scope)){
			foreach($scope as $idx => $perm_key){
				if(!$this->has_permission($perm_key)){
					return false;
				}
			}
			return true;
			
		}
		
		$req_perm_key = strtoupper($scope);
		
		$keys = array_keys($this->perms);
		
		foreach($keys as $idx => $perm_key){
			if(strtoupper($perm_key) == $req_perm_key){
				if ($this->perms[$perm_key]['value'] == '1' || $this->perms[$perm_key]['value'] == true)
				{
					return true;
				} else {
					return false;
				}
			}
		}
		
		return false;
	}
	
	function table($name){
		if( isset($this->tables[$name]) && !empty($this->tables[$name]))
			return $this->tables[$name];
		return $name;
	}
	
	function field($name){
		if(isset($this->fields[$name]))
			return $this->fields[$name];
		return $name;
	}
}