<?php

class AbleSMS_SMSProvider extends SMSProvider
{
	public function getProviderId()
	{
		return 'com.able-sms.able1';
	}
	
	public function send($message){
		$data = NULL;
		
		$p = array('Username'=>$this->_username,
				"Password"=>$this->_password,
				"Message"=>$message->content,
				'Hex'=>'',
				'CountryCode'=>$message->countryCode,
				'UserDefineNo'=>$message->from,
				'Telephone'=> $message->to,
		);
		
		if(!empty($message->scheduleTime)){
			$p['Date'] = date("YYYY/MM/DD hh:mm:ss Z", is_string($message->scheduleTime) ? strtotime($message->scheduleTime): $message->scheduleTime);
		}

		$uri = $this->getSendMessageURI().'?'.http_build_query($p);
		
		$CI = &get_instance();
		$CI->load->library('curl');
		log_message('debug','AbleSMS_SMSProvider/send, $uri = '.$uri);
		
		$res = new SMSMessageResponse;
		$res->responseString = (string) $CI->curl->simple_get($uri);

		log_message('debug','AbleSMS_SMSProvider/send, responseString='.var_export($res->responseString,TRUE));
		if(function_exists('simplexml_load_string')){
			$xml = @simplexml_load_string($res->responseString);
			log_message('debug','AbleSMS_SMSProvider/execute, result = '.print_r($xml,true));
			
			$result = $xml->ReturnValue;
			$state = (int) $result->State;
			
			$res->units = (int) $result->Count;
			$res->identifer = (string) $result->ResponseID;
			$res->status = $this->stateCodeToStatus($state);
			
			return $res;
			
		}
		
		return $str;
	}

	protected function stateCodeToStatus($state){
		if($state == '1'){
			return 'complete';
		}elseif($state == '0'){
			return 'missing-values';
		}elseif($state == '10'){
			return 'unauthorized';
		}elseif($state == '20' || $state == '30'){
			return 'message-too-long';
		}elseif($state == '40'){
			return 'to-address-too-long';
		}elseif($state == '60'){
			return 'incorrect-country-code';
		}elseif($state == '70'){
			return 'no-balance';
		}elseif($state == '80'){
			return 'datetime-incorrect';
		}elseif($state == '100'){
			return 'server-error';
		}
		return 'unknown';
	}
	
	protected function getSendMessageURI(){
		return "http://able1.able-sms.com:8080/service/smsapi5.asmx/SendMessage";
	}
	
	protected function getCheckMessageURI(){
		return NULL;
	}
}