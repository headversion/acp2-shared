<?php


class InterSoft_SMSProvider extends SMSProvider
{
	public function getProviderId(){
		return 'com.intersoft.sms';
	}
	
	public function send( $message){
		$data = NULL;
		
		$p = array(
				'Username'=>$this->_username,
				"Password"=>$this->_password,
				"Message"=>$message->content,
				'MobileNumber'=> $message->to,
				'ScheduleTime'=>'',
		);

		$uri = $this->getSendMessageURI();
		
		$CI = &get_instance();
		$CI->load->library('curl');
		
		log_message('debug','InterSoft_SMSProvider/send, $uri = '.$uri);
		$res = new SMSMessageResponse;
		$res->responseString = (string) $CI->curl->simple_post($uri,$p);
		
		$res->responseString = $CI->curl->getResponse();
		log_message('debug','InterSoft_SMSProvider/send, responseString = '.nl2br(htmlentities($res->responseString)));
		
		if(function_exists('simplexml_load_string')){
			$xml = simplexml_load_string($res->responseString);
			log_message('debug','InterSoft_SMSProvider/send, result = '.print_r($xml,true));
			
			//$xml->registerXPathNamesapce('soap','http://schemas.xmlsoap.org/soap/envelope/');
			//foreach($xml->xpath('//MessageResult') as $envelope) {
			if(isset($xml->Accept)){
				$is_accept = (string) $xml->Accept;
				if($is_accept == 'false'){
					$res->status = 'fail';
					$res->identifer =  NULL;
					$res->reason = (string) $xml->FailReason;
					log_message('error','InterSoft_SMSProvider/send, Reason='.$res->reason);
				}else{
					$res->units = intval((string) $xml->MessageLength);
					$res->identifer = (string) $xml->MessageID;
					log_message('debug','InterSoft_SMSProvider/send, MessageID='.$res->identifer.', Units='.$res->units);
				}
			}
			
			if(!empty($res->identifer)){
				return $this->checkStatus($res);
			}
			
			return $res;
		}
		
		return $str;
	}

	public function checkStatus($identifier){
		
		$res = new SMSMessageResponse;
		$res->raw = '';
		if(is_object($identifier)){
			$res = $identifier;
			$identifier = $res->identifer;
		}
		
		$uri = $this->getCheckMessageURI();
		$p = array(
			'Username'=>$this->_username,
			"Password"=>$this->_password,
			"MessageID"=>$identifier,
		);
		$CI = &get_instance();
		$CI->load->library('curl');
		
		$CI->curl->simple_post($uri,$p);
		
		$str = $CI->curl->getResponse();
		log_message('debug','InterSoft_SMSProvider/send, responseString = '.nl2br(htmlentities($str)));
		
		if(function_exists('simplexml_load_string')){
			$xml = simplexml_load_string($str);
			log_message('debug','InterSoft_SMSProvider/check, result = '.print_r($xml,true));
				$is_accept = (string) $xml->Accept;
				if($is_accept == 'false'){
					$res->status = 'fail';
					$res->identifer =  NULL;
					$res->reason = (string) $xml->FailReason;
					log_message('error','InterSoft_SMSProvider/send, Reason='.$res->reason);
				}else{
					$res->status = $this->translateStatus( (string) $xml->MessageStatus );
					$res->scheduledTime = (string) $xml->MessageScheduledTime;
					$res->sentTime = (string) $xml->MessageSentTime ;
					$res->receiptTime = (string) $xml->MessageReceiptTime;
				}
			/*
			$xml->registerXPathNamesapce('soap','http://schemas.xmlsoap.org/soap/envelope/');
			foreach($xml->xpath('//soap:Envelope') as $envelope) {
				$res->status = $this->translateStatus( (string) $envelope->xpath('//soap:Body/CheckMessageStatusResponse/MessageStatus/MessageStatus') );
			
			}
			//*/
			return $res;
		}
		return $str;
	}
	
	protected function translateStatus($state){
		
		if($state == 'Success'){
			return 'complete';
		}elseif($state == 'Sending'){
			return 'sending';
		}elseif($state == 'Scheduled'){
			return 'scheduled';
		}elseif($state == 'Queueing'){
			return 'queueing';
		}elseif($state == 'Partial'){
			return 'partial';
		}
		return 'unknown';
	}
	
	protected function getSendMessageURI(){
		return "http://login.speedfax.net/SMSWS/sms.asmx/AddMessage";
	}
	
	protected function getCheckMessageURI(){
		return "http://login.speedfax.net/SMSWS/sms.asmx/CheckMessageStatus";
	}
}