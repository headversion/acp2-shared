<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


class SMS 
{
	
	private $provider;
	private $response = null;
	private $message = null;
	private $CI;
	
	function __construct($cfg=false){
	
	

		$this->CI =& get_instance();
		
		if(!is_array($cfg) || empty($cfg)){
			$CI = &get_instance();
			$CI->load->config('sms');
			
			$cfg = array();
			if($CI->config->item('sms_provider')!=NULL)
				$cfg['provider'] = $CI->config->item('sms_provider');
			if($CI->config->item('sms_username')!=NULL)
				$cfg['username'] = $CI->config->item('sms_username');
			if($CI->config->item('sms_password')!=NULL)
				$cfg['password'] = $CI->config->item('sms_password');
		}
		
		if(isset($cfg['sms_provider'])){
			$cfg['provider'] = $cfg['sms_provider'];
		}
		
		if(isset($cfg['sms_username'])){
			$cfg['username'] = $cfg['sms_username'];
		}
		if(isset($cfg['sms_password'])){
			$cfg['password'] = $cfg['sms_password'];
		}
		
		$this->initiailize($cfg);
		log_message('debug','SMS Class Initialized');
	}
	
	function initiailize ($cfg=false){
	
			log_message('debug','SMS: initialized with config = '.print_r($cfg,true));
		if(isset($cfg['provider'])){
			$this->provider = null;
			if(is_string($cfg['provider'])){
				
				$c_name = $cfg['provider'];
				
				$c_opts = isset($cfg['provider_options']) ? $cfg['provider_options'] : NULL;
				
				$file = (dirname(__FILE__).DIRECTORY_SEPARATOR.'drivers'.DIRECTORY_SEPARATOR. $c_name.'.'.EXT);
					log_message('debug','SMS: test class path at '.$file);
				if(!file_exists($file)){
					$file = (dirname(__FILE__).DIRECTORY_SEPARATOR.'drivers'.DIRECTORY_SEPARATOR. strtolower($c_name.'.'.EXT));
					log_message('debug','SMS: test class path at '.$file);
				}
				
				if(file_exists($file)){
					require_once $file;
					$this->provider = new $c_name($c_opts);
				}else{
					log_message('debug','SMS: try to use system library');
					$this->CI->load->library('sms/drivers/'.$c_name, $c_opts, 'sms_provider');
					$this->provider = &$this->CI->sms_provider;
				}
		
				
			}elseif(is_object($cfg['provider']) && is_subclass_of('SMSProvider')){
				$this->provider = $cfg['provider'];
			}
		}
		if(isset($cfg['username']) && !empty($this->provider))
			$this->provider->setUsername($cfg['username']);
		
		if(isset($cfg['password']) && !empty($this->provider))
			$this->provider->setPassword($cfg['password']);
		
		if(empty($this->message)){
			$this->clear();
		}
		if( !$this->provider || empty($this->provider)){
			
			log_message('debug','SMS: No provider was provided.');
			return FALSE;
		}
	}
	
	public function clear(){
		$this->response = NULL;
		$this->message = new SMSMessage;
	}
	
	public function getMessage(){
		return $this->message;
	}
	
	public function getResponse(){
		return $this->response;
	}
	
	public function from($address, $name){
		$this->message->from = $address;
		$this->message->fromName = $name;
	}
	
	
	public function subject($subject){
		$this->message->subject = $subject;
	}
	
	public function content($content){
		$this->message->content = $content;
	}
	
	public function to($address){
		$this->message->to = $address;
	}
	
	public function send(){
		if( !$this->provider || empty($this->provider)){
			
			log_message('error','SMS: No provider selected. Cannot send message.');
			return FALSE;
		}
		
		$this->response =  $this->provider->send( $this->message );
		return $this->response;
	}
}

class SMSMessage
{
	public $identifer = NULL;
	public $subject = '';
	public $content = '';
	public $countryCode = '852';
	public $to = '';
	public $from = '';
	public $fromName = '';
	
	public function __construct($opts=false){
		if(isset($opts['identifer'])){
			$this->identifer = $opts['identifier'];
		}
		if(isset($opts['subject'])){
			$this->title = $opts['subject'];
		}
		if(isset($opts['message'])){
			$this->message = $opts['message'];
		}
		if(isset($opts['to'])){
			$this->to = $opts['to'];
		}
		if(isset($opts['from'])){
			$this->from = $opts['from'];
		}
		if(isset($opts['fromName'])){
			$this->fromName = $opts['fromName'];
		}
	}
}

class SMSMessageStatus
{
	public $identifer = NULL;
	public $status = 'unknown';
}
class SMSMessageResponse extends SMSMessageStatus
{
	public $units = 0;
	public $responseString = NULL;
}

class SMSProvider {
	
	var $_username;
	var $_password;
	
	public function __construct(){
		
		log_message('debug','SMSProvider['.$this->getProviderId().'] Class Initialized');
	}
	
	public function getProviderId(){
		return 'UnknownSMSProvider';
	}
	
	public function setUsername($username){
		$this->_username = $username;
		return $this;
	}
	
	public function setPassword($password){
		$this->_password = $password;
		return $this;
	}
	
	public function send( $message ){
		return NULL;
	}
	
	protected function getSendMessageURI(){
		return NULL;
	}
} 



