<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// CodeIgniter i18n library by Jérôme Jaglale
// http://maestric.com/en/doc/php/codeigniter_i18n
// version 6 - April 20, 2009

class HC_Config extends CI_Config {

	function site_url($uri = '', $localize = true)
	{	
		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}
		
		if (function_exists('get_instance') && $localize)		
		{
			$CI =& get_instance();
			$uri = $CI->lang->localize_url($uri);
			
			$tmp_locale_info = $CI->lang->parse_url($uri);
			
			
			if(isset($tmp_locale_info['locale'])){
				$locale_info = $CI->lang->get_locale($tmp_locale_info['locale']);
				
				$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
				if(substr($host,0,4) == 'www.')
				$host = substr($host,4);
				
				if(isset($locale_info['host']) && !empty($host) && $locale_info['host'] != $host){
					$new_uri = $locale_info['host'].'/'.$uri;
					$new_uri = 'http://'.preg_replace('/\/+/', '/', $new_uri);
					if(substr($new_uri,-1,1) == '/')
						$new_uri = substr($new_uri,0, strlen($new_uri)-1);
					
					//log_message('debug','MY_Config/site_url, uri="'.$uri.'", '.$new_uri);
					return $new_uri;
				}
			}
		}

		return parent::site_url($uri);
	}
		
}
// END MY_Config Class

/* End of file MY_Config.php */
/* Location: ./system/application/libraries/MY_Config.php */