<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HC_Controller extends CI_Controller
{
	
	// Please change to new method $this->_ext('html') to check the extension group
	var $supported_data_extension = array('json', 'plist', 'xml');
	var $supported_view_extension = array('', 'htm', 'html', 'js');
	var $supported_html_extension = array('', 'htm', 'html');

	var $supported_extensions = array(
		'html'=> array('', 'htm', 'html'),
		'view'=> array('', 'htm', 'html', 'js'),
		'asset'=> array('css', 'js'),
		'data'=> array('json', 'plist', 'xml'),
	);
	
	function __construct(){

		if(!isset($_SERVER['REMOTE_ADDR']))
			$_SERVER['REMOTE_ADDR'] = '0.0.0.0';

		parent::__construct();
		
		$this->_init_session();
		
		
		$req_sess_debug = $this->input->get('session-debug');
		if (!empty($req_sess_debug) ) {
			$this->config->set_item('debug_mode', $req_sess_debug);
			$this->session->set_userdata('debug_mode', $req_sess_debug);
		}elseif( isset($this->session) && $this->session->userdata('debug_mode') !== NULL){
			$this->config->set_item('debug_mode',$this->session->userdata('session-debug'));
		}
		
		$this -> load -> helper('datetime');
		$this -> load -> helper('data');
		$this -> load -> helper('api');
		$this -> load -> helper('render');
		$this -> load -> library('render');
	}

	protected function _init_session(){
		// Feature: Native PHP Session
		$cfg=  array();
		if($this->input->get('token') != NULL){
			$cfg['token'] = $this->input->get('token');
		}
		$this->load->library('Native_session',$cfg,'session');
	}
	
	// return FALSE which is Allowed.
	public function _restrict($scope = NULL,$redirect=true){
		return FALSE;
	}
	
	public function _permission_denied($scope=NULL){
		return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG, 401, NULL);
	}
	
	public function _is_debug(){
		if($this->input->get('debug') == 'no'){
			return FALSE;
		}
		if ( ENVIRONMENT == 'development' && isset($this->config) && $this->config->item('debug_mode') == 'yes') {
			return TRUE;
		}
		if( defined('PROJECT_DEBUG_KEY') && $this->input->get('debug') == PROJECT_DEBUG_KEY)
			return TRUE;
		
		return FALSE;
	}

	public function _is_ext($group='html'){
		$group_exts = $this->supported_extensions[$group];

		return $this->uri->is_extension($group_exts);
	}

	public function _is_extension($group='html'){
		$group_exts = $this->supported_extensions[$group];

		return $this->uri->is_extension($group_exts);
	}

	public function _system_error($code, $message = 'Unknown system error.', $status=200, $data = NULL){
		$case_id = uniqid('SER-');

		$vals = array(
			'uri_string'=>uri_string(),
			'uri_query'=>uri_query(),
			'post'=>$_POST,
			'get'=>$_GET,
			'data'=>$data,
		);

		// Log data into system folder.
		error_log('ErrorReport['.$case_id.'] info: '.print_r($vals, true));

		$message = '[Case ID: '.$case_id.']<br />'.$message;


		$data['case_id'] = $case_id;

		$this->_error($code, $message, $status, $data);
	}
	
	public function _error($code, $message = '', $status = 200, $data=NULL) {

		if ($this->_is_ext('data'))
			return api_error($code, $message, $status,$data);
		return show_error($message, $status, 'Exception - Code ' . $code);
	}
	

	public function _show_404($message = 'Un-specified 404 error.') {
		$this->output->set_header('HTTP/1.1 404 Page not found');

		// reset render view's extension
		$this->uri->extension('');

		if ($this->_is_debug()) {
			header("Content-type: text/plain");
			print "Message: ".$message."\r\n";
			print"Class: " . get_class($this) . "\r\n";
			print"Queries: " . print_r($this->db->queries, true) . "\r\n";
			print"Segments: " . print_r($this->uri->segments, true) . "\r\n";
			print"Redirected Segments: " . print_r($this->uri->rsegments, true) . "\r\n";
			print"Backtrace: " . "\r\n" ; debug_print_backtrace() ; print "\r\n";
			return;
		}

		log_message('error','404 ERROR at '.uri_string().'. Message returned: '.$message.'');

		if (isset($this->acp_auth) && $this->acp_auth->is_login()) {
			return $this->_render('404');
		}

		return $this->_render('404', NULL, 'blank');
	}

	public function _api($vals, $format='json'){
		if($this->uri->is_extension(''))
			$this->uri->extension($format);

		return api_output($vals);
	}

	protected function _render($view, $vals = false, $layout = false, $theme = false) {


		if (!is_array($vals)) {
			$vals = array();
		}

		if( $this->_is_debug() ){
			$this->render->compress_js = false;
		}

		if (!isset($vals['is_dialog'])) {
			$vals['is_dialog'] = false;
		}

		if (!$theme) {
			$theme = $this->config->item('theme');
		}

		if (!$layout) {
			$layout = 'default';
		}

		if (!$this->_is_ext('html')) {
			$req_ext   = $this->uri->extension();
			$view_path = NULL;

			if (empty($view_path)) {
				$view_path = 'themes/' . $theme . '/' . $view . '.' . $req_ext . EXT;
				if (!file_exists(APPPATH . 'views/' . $view_path)) {
					$view_path = NULL;
				}
			}
			if (empty($view_path)) {
				$view_path = $view . '.' . $req_ext . EXT;
				if (!file_exists(APPPATH . 'views/' . $view_path)) {
					$view_path = NULL;
				}
			}
			if (empty($view_path)) {
				$view_path = 'themes/' . $theme . '/' . $view . EXT;
				if (!file_exists(APPPATH . 'views/' . $view_path)) {
					$view_path = NULL;
				}
			}
			if (empty($view_path)) {
				$view_path = $view . EXT;
				if (!file_exists(APPPATH . 'views/' . $view_path)) {
					$view_path = NULL;
				}
			}
			if (!empty($view_path)) {
				if ($this->uri->is_extension('js')) {
					$this->output->set_content_type('text/javascript');
				} elseif ($this->uri->is_extension('xml', 'plist')) {
					$this->output->set_content_type('text/xml');
				} else {

					$this->output->set_content_type('text/plain');
				}

				return $this->load->view($view_path, $vals);
			}
			return $this->_show_404();
		}

		$this->load->helper('form');

		$view_path = '';
		$view_theme_path = '';
		if (empty($view_path) && !empty($view)) {
			$theme_path = 'themes/' . $theme . '/';
			$view_theme_path = $theme_path;
			$view_path = 'themes/' . $theme . '/' . $view . EXT;
			if (!file_exists(APPPATH . 'views/' . $view_path)) {
				$view_path = NULL;
				$theme_path = '';
				$view_theme_path = '';
			}
		}
		if (empty($view_path) && !empty($view)) {
			$view_path = $view . EXT;
			if (!file_exists(APPPATH . 'views/' . $view_path)) {
				$view_path = NULL;
			}
		}

		$layout_path = '';

		if (empty($layout_path)) {
			$theme_path = 'themes/' . $theme . '/';
			$layout_path = 'themes/' . $theme . '/layouts/' . $layout . EXT;
			if (!file_exists(APPPATH . 'views/' . $layout_path)) {
				$layout_path = NULL;
				$theme_path = '';
			}
		}
		if (empty($layout_path)) {
			$theme_path = 'themes/' . $theme . '/';
			$layout_path = 'themes/' . $theme . '/' . $layout . EXT;
			if (!file_exists(APPPATH . 'views/' . $layout_path)) {
				$layout_path = NULL;
				$theme_path = '';
			}
		}
		if (empty($layout_path)) {
			$theme_path = 'themes/' . $theme . '/';
			$layout_path = 'themes/' . $theme . '/default' . EXT;
			if (!file_exists(APPPATH . 'views/' . $layout_path)) {
				$layout_path = NULL;
				$theme_path = '';
			}
		}
		if (empty($layout_path)) {
			$layout_path = 'layouts/' . $layout . EXT;
			if (!file_exists(APPPATH . 'views/' . $layout_path)) {
				$layout_path = NULL;
			}
		}
		if (empty($layout_path)) {
			$layout_path = $layout;
		}

		$vals['is_debug'] = $this->_is_debug();
		$vals['view']      = $view;
		$vals['view_path'] = $view_path;
		$vals['theme_path'] = $theme_path;
		$vals['view_theme_path'] = $view_theme_path;
		$vals['theme']     = $theme;
		$vals['layout']    = $layout;

		$this->load->view($layout_path, $vals);
	}
}
