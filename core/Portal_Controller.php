<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portal_Controller extends HC_Controller {

	var $record_status_code       = 1;
	var $record_is_live			  = 1;
	

	var $is_refresh               = false;

	function __construct() {
		parent::__construct();

		
		//*/

		$this->load->helper('api');
		$this->load->helper('cache');

		
		$this->load->config('ph');
		$this->load->helper('post');


		//*/

		// pass 'refresh=yes' to rebuild all material.
		$this->is_refresh = $this->input->get('refresh') == 'yes' || $this->input->get('refresh') == 'true';

		$this->config->set_item('is_debug', $this->_is_debug());
		$this->config->set_item('is_refresh', $this->is_refresh);

		log_message('debug','Portal_Controller/is_refresh='. ($this->is_refresh ? 'Y':'N'));

		$this->load->model('pref_model');

		// load db pref into sys config
		$locale = $this->lang->locale();
		$pref_fields = array('site_name','site_keywords','site_description','sharing_title','sharing_description');
			
		foreach($pref_fields as $field){
			$val = $this->pref_model->locale_item($locale, $field);

			$this->config->set_item($field, $val);
		}

		if($this->config->item('site_keywords') != NULL){
			$this->render->set_meta_content('keywords', $this->config->item('site_keywords'));
		}

		if($this->config->item('site_description') != NULL){
			$this->render->set_meta_content('description', $this->config->item('site_description'));
		}


		$this->config->set_item('preview_mode', false);
		if (defined('PREVIEW_MODE') && PREVIEW_MODE) {
			$this->config->set_item('preview_mode', true);

			// Feature: Authentication
			$this->load->config('acp');
			$cfg = array(
				'table' => 'admins',
				'path'  => array(
					'login' => 'acp/auth/signin',
				),
				'field' => array(
					'loginkey' => 'login_name',
					'password' => 'login_pass',
				),
				'activiate'          => 'admin',
				'encrypt_config_key' => 'acp_encryption_key',
			);
			$this->load->library('LMS_Auth', $cfg, 'acp_auth');

			if (!$this->acp_auth->is_login()) {
				$this->_permission_denied();
			}

			$this->record_status_code = '1';
			$this->record_is_live = '0';
		}

	}

	function _restrict($scope = NULL, $redirect = true) {

		if ($redirect && isset($this->member_aut) && $this->member_auth->restrict()) {
			return true;
		}

		if (!$redirect && isset($this->member_aut) && !$this->member_auth->is_login()) {
			return true;
		}

		return false;
	}

	function _picture_mapping($file_row,$group='file',$size='thumbnail',$subpath = false,$options = false,&$image_info=false){
		if(is_array($file_row) && !isset($file_row['id'])){
			$outputs = array();
			foreach($file_row as $file){
				$_image_info = false;
				$image_output = $this->_picture_mapping( $file, $group,$size,$subpath,$options,$_image_info);
				if(!empty($image_output)){
					$outputs[] = $image_output;
					$image_info[] = $_image_info;
				}
			}
			return $outputs;
		}
		$dest_path = '';
		$dest_path_seg = array();
		
		if(!empty($subpath)){
			if(is_array($subpath)){
				$dest_path = implode('/', $subpath);
				$dest_path_seg = $subpath;
			}else{
				$dest_path = $subpath;
				$dest_path_seg = explode('/', $dest_path);
			}
		}

		if(!$image_info) 
			$image_info = array();

		//log_message('debug','Portal_controller/_picture_mapping, build image for '.$file_row['id'].' at subpath '.$dest_path.'. imagegroup='.$group.', size='.$size);

		$url = picture_url($file_row,$group,$size,$this->is_refresh, $image_info, 'files',$dest_path_seg);
		if(empty($url)){
			return NULL;
		}
		$output = array();
		$output['url'] = $url;
		$output['width'] = $image_info['width'];
		$output['height'] = $image_info['height'];
		//$output['_raw'] = $image_info;
		return $output;
	}
}

