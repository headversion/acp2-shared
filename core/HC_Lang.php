<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');  

class HC_Lang extends CI_Lang
{
	var $lang_code = '';
	var $country_code = NULL;
	var $locale_code = '';
	var $locale_def = 'en';
	var $standard_locale_code = 'en_US';
	var $extra_code = NULL;
	public $requested_locale_code = NULL;
	public $host = NULL;
	
	public $uri_has_locale = false;
	public $uri_is_alias_locale = false;
	
	public $supported_locales = array();
	
	private $_supported_locale_codes = array();
	
	function __construct()
	{
		parent::__construct();
		
		$this->__init();
	}

	public function has_available_locale()
	{
		$locales = $this->get_available_locale_keys();
		if(count($locales)>0){
			return TRUE;
		}
		return FALSE;
	}

	public function get_available_locale_keys()
	{
		$locales = array();
		foreach($this->supported_locales as $locale_key => $locale_info){
			if(is_string($locale_info)) continue;
			$locales[] = $locale_key;
		}
		return $locales;
	}

	public function get_all_locale_keys(){
		return array_keys($this->supported_locales);
	}

	public function get_locale_info($locale_code){
		if(isset($this->supported_locales[$locale_code]))
			return $this->supported_locales[ $locale_code ];
		return NULL;
	}
	
	public function url_prefix($locale_code=false)
	{
		if(!$locale_code) $locale_code = $this->locale_code;
		
		$locale_info = $this->get_locale($locale_code);
		
		if($locale_info['locale'] != $this->locale_def){
			return $this->locale_code.'/';
		}
		return '';
	}
	
	protected function parse_uri()
	{
		global $URI,$CFG,$RTR;
		
		
		$def_loc = $CFG->item('locale_default');
		if($def_loc){
			$this->locale_def = $def_loc;
		}
		
		$cur_loc = $URI->segment(1);
		if(!$cur_loc || !$this->valid_locale_str($cur_loc)){
			//log_message('debug','MY_Lang/parse_uri: non-locale format: ' . $cur_loc);
			return NULL;
			//$cur_loc = $this->locale_def;
		}
		//log_message('debug','MY_Lang/parse_uri: locale format: ' . $cur_loc);
		
		$locale_info = $this->parse_locale($cur_loc);
		if(!isset($locale_info['locale'])){
			$locale_info = $this->parse_locale( $def_loc );
		}
		return $locale_info;
	}
	
	public function parse_url($str=''){
		$offset = substr($str,0,1) == '/' ? 1:0;
		$segments = explode('/',$str);
		
		//log_message('debug','MY_Lang/parse_url: ["'.$str.'"/'.$offset.']='.$segments[$offset]);
		
		return $this->parse_locale($segments[$offset]);
	}
	
	/**
	 * Parse the localizcation information by string 
	 *
	 * @return	mixed
	 */
	public function parse_locale($str=''){

		if(preg_match("/^([a-z]{2}|master)[\_\-]([a-zA-Z]{2})$/",$str,$matches)){
		
			return array(
				'locale'=>strtolower($str),
				'lang'=>strtolower($matches[1]),
				'country'=>strtolower($matches[2]),
			);
		}elseif(preg_match("/^([a-z]{2}|master)$/",$str,$matches)){
			return array(
				'locale'=>strtolower($str),
				'lang'=>NULL,
				'country'=>strtolower($matches[1]),
			);
		}else{
			//log_message('info','MY_Lang/parse_locale# non-locale format:'.$str);
		}
		
		return NULL;
	}
	
	function get_locale($str=''){
		$str = strtolower($str);
		$str_info = $this->parse_locale($str);
		//log_message('debug','MY_Lang/get_locale='.$str.',info='.print_r($str_info,true));
		
		if(!isset($this->supported_locales[ $str_info['locale'] ])) return NULL;
		
		$data = array();
		$data['locale'] = $str_info['locale'];
		$data['lang'] = $str_info['lang'];
		$data['country'] = $str_info['country'];
		$data['language'] = $str_info['lang'];
		$data['extra'] = isset($str_info['extra']) ? $str_info['extra'] : NULL;
		
		$def_data = $this->supported_locales[ $str_info['locale'] ];
		
		
		if(is_string($def_data)){
			$def_data = $this->supported_locales[ $def_data ];
		}
		if(is_array($def_data)){
			if(isset($def_data['lang']))
				$data['lang'] = $def_data['lang'];
			if(isset($def_data['country']))
				$data['country'] = $def_data['country'];
			if(isset($def_data['locale']))
				$data['locale'] = $def_data['locale'];
			if(isset($def_data['s_locale']))
				$data['s_locale'] = $def_data['s_locale'] ;
			if(isset($def_data['language']))
				$data['language'] = $def_data['language'] ;
			if(isset($def_data['host']))
				$data['host'] = $def_data['host'] ;
			if(isset($def_data['extra']))
				$data['extra'] = $def_data['extra'] ;
		}
		return $data;
	}
	
	public function valid_locale_str($str=''){
		return preg_match("/^([a-z]{2}|master)[\_\-]([A-Z]{2}|[a-z]{2})$/",$str) || preg_match("/^([a-z]{2}|master)$/",$str);
	}
	
	public function has_locale($str='')
	{
		$locale_info = $this->parse_url($str);
		if(isset($locale_info['locale'])){
			return in_array($locale_info['locale'], $this->_supported_locale_codes);
		}
		return FALSE;
	}
	
	public function localize_url($str){
		if(!empty($str) && $this->has_locale($str)){
			//log_message('debug','MY_Lang/localize_url: has a valid localized url for "'.$str.'"');
			return $str;
		}else{
			//log_message('debug','MY_Lang/localize_url: not a valid localized url for "'.$str.'"');
		}
		$prefix = $this->url_prefix();
		if(substr($prefix,-1,1) !='/' && substr($str,0,1)!='/')
			$prefix.='/';

		//log_message('debug','Lang/localize_url:'.print_r(compact('prefix','str'),true));
		return $prefix.$str;
	}
	
	var $query_pattern = '#(\{lang\:([a-zA-Z\-\_0-9\.\s]+)\})#';
	public function line($line = '', $show_original_text=TRUE)
	{

		if(preg_match_all($this->query_pattern,$line,$matches)){
			$text = $line ;
			for($i = 0; $i < count($matches[0]); $i++){
				$pattern = $matches[1][$i];
				$key = $matches[2][$i];
				//print_r(compact('pattern','key'));
				$new_str = $this->line($key, $show_original_text);
				//if($new_str == $key) continue;
				$text = str_replace($pattern, $new_str, $text);
			}

			return $text;
		}

		$value = parent::line($line);

		// Because killer robots like unicorns!
		if ($value === FALSE && $show_original_text)
		{
			return $line;
		}

		return $value;
	}
	
	public function lang(){
		return $this->lang_code;
	}
	
	public function country(){
		return $this->country_code;
	}
	
	public function locale(){
		return $this->locale_code;
	}
	
	public function standard_locale(){
		return $this->standard_locale_code;
	}
	
	public function extra($name){
		return isset($this->extra_code[$name]) ? $this->extra_code[$name] : $this->standard_locale_code;
	}
	
	public function requested_locale(){
		return $this->requested_locale_code;
	}
	
	public function is_alias(){
		return $this->uri_is_alias_locale;
	}

	public function is_supported($locale, $alias= true){
		if(is_string($locale)){
			$is_available = isset($this->supported_locales[ $locale ]);
			if($is_available && !$alias)
				return !is_string($this->supported_locales[ $locale ]);
			return $is_available;
		}
		return FALSE;
	}
	
	protected function __init(){
	
		global $URI,$CFG,$RTR;
		
		
		$CFG->load('language');
		$this->supported_locales = $CFG->item('supported_locales');
		
		if(is_array($this->supported_locales))
			$this->_supported_locale_codes = array_keys($this->supported_locales);
		
		
		$locale_info = $this->parse_uri();
		
		if(isset($locale_info['locale'])){
			$this->lang_code= $locale_info['lang'];
			$this->country_code= $locale_info['country'];
			$this->locale_code= $locale_info['locale'];
			$this->uri_has_locale = true;
		}
		
		
		if($this->uri_has_locale){
			//log_message('debug','MY_Lang# before re-group segments:'.print_r($URI->segments,true));
			//log_message('debug','MY_Lang# before re-group rsegments:'.print_r($URI->rsegments,true));
			$this->requested_locale_code = $URI->segments[1];
			
			// replace uri's segment for correct
			$new_segments = array();
			$counter = 1; // URI segment is started from 1
			// first node must be supported locale code
			for($i = 2; $i <= count($URI->segments); $i++){
				$new_segments[$counter++] = $URI->segments[$i];
			}

			//log_message('debug','MY_Lang# re-group segments:'.print_r($new_segments,true));

			$URI->segments = $new_segments;
			$URI->uri_string = implode("/", $new_segments);
			
			
			if(is_array($this->supported_locales) && !in_array($this->locale_code, array_keys($this->supported_locales))){
				
				//log_message('debug','MY_Lang# unsupported locale:'.$this->locale_code);
				
				return show_404();	
			}
			
			$this->uri_is_alias_locale = isset($this->supported_locales[$this->requested_locale_code]) && is_string($this->supported_locales[$this->requested_locale_code]);
			
			$locale_data = $this->get_locale($locale_info['locale']);
			
			if(is_array($locale_data)){
				if(isset($locale_data['lang']))
					$this->lang_code = $locale_data['lang'];
				if(isset($locale_data['country']))
					$this->country_code = $locale_data['country'];
				if(isset($locale_data['locale']))
					$this->locale_code = $locale_data['locale'];
				if(isset($locale_data['s_locale']))
					$this->standard_locale_code = $locale_data['s_locale'];
				if(isset($locale_data['host']))
					$this->host = $locale_data['host'];
				if(isset($locale_data['extra']))
					$this->extra_code = $locale_data['extra'];
				$CFG->set_item('language', $locale_data['language'] );
			}
		}else{
			$locale_data = $this->get_locale($this->locale_def);
			
			if(is_array($locale_data)){
				if(isset($locale_data['lang']))
					$this->lang_code = $locale_data['lang'];
				if(isset($locale_data['country']))
					$this->country_code = $locale_data['country'];
				if(isset($locale_data['locale']))
					$this->locale_code = $locale_data['locale'];
				if(isset($locale_data['s_locale']))
					$this->standard_locale_code = $locale_data['s_locale'];
				if(isset($locale_data['host']))
					$this->host = $locale_data['host'];
				if(isset($locale_data['extra']))
					$this->extra_code = $locale_data['extra'];
				$CFG->set_item('language', $locale_data['language'] );
			}
		}
	}
}