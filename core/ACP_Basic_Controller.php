<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/ACP_Controller.php';

if(!class_exists('MY_Controller')){
	class MY_Controller extends ACP_Controller{}
}

class ACP_Basic_Controller extends MY_Controller
{
	// string
	var $model = 'target_model';
	var $tree = 'target';
	var $view_prefix = '';
	var $view_scope = 'target';
	var $view_type = 'post';
	var $page_header = 'target_heading';
	var $deep = 0;
	var $localized = FALSE;
	var $locale_fields = array('title','description','content','parameters','status');
	var $sorting_fields = array('priority','id','publish_date','create_date','modify_date');
	var $editable_fields = NULL; // default all fields could be edited.
	var $sorting_direction = 'asc';
	var $keyword_fields = array('id','title','content','description');
	var $cache_prefix = NULL;
	var $action = 'index';
	var $has_record_view = FALSE;
	var $home_related = FALSE;

	var $view_path_prefix = NULL;
	var $endpoint_path_prefix = NULL;

	protected $_target_model = NULL;

	function __construct(){
		parent::__construct();

		$model_name = $this->model;
		
		$this->load->model($model_name);
		$this->_target_model = $this->$model_name;

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>', '</strong></div>');


		$this->config->set_item('main_menu_selected', $this->tree);
		if($this->localized){
			$this->load->model('text_locale_model');
		}

		if($this->home_related){
			$this->load->model('home_content_model');
		}

		// if controller does not contain path prefix, combined by configured version
		if(empty($this->view_path_prefix)) 
			$this->view_path_prefix = $this->view_prefix.$this->view_scope.'/'.$this->view_type;

		// if controller does not contain path prefix, combined by configured version
		if(empty($this->endpoint_path_prefix)) 
			$this->endpoint_path_prefix = $this->view_prefix.$this->view_scope.'/'.$this->view_type;
	}

	protected function _get_default_vals($action = 'index'){
		$vals= array();

		if($action == 'search'){

			$vals['paging'] = array();
			$vals['paging']['offset'] = 0;
			$vals['paging']['total'] = 0;
			$vals['paging']['limit'] = 0;
			$vals['paging']['page'] = 0;
			$vals['paging']['total_page'] = 0;
			$vals['data'] = array();
		}
		return $vals;
	}

	protected function _shorten_text($val, $length=200, $tail = '...', $encoding = 'UTF-8'){
		$size = mb_strlen($val, $encoding);
		if($size > $length){
			return mb_substr($val, 0, $length - strlen($tail), $encoding);
		}
		return $val;
	}

	protected function _available_locales(){
		return $this->lang->get_available_locale_keys();
	}
	
	protected function _mapping_row($raw_row){
		$row = array();
		$row['id'] = $raw_row['id'];

		if(isset($raw_row['slug'])){
			$row['slug'] = $raw_row['slug'];
		}
		if(isset($raw_row['priority'])){
			$row['priority'] = intval($raw_row['priority']);
		}

		// get localized content
		if($this->localized){
			$loc_options = array(
				'_field_based'=>'locale',
				'_select'=>'id,is_live,locale,title,content,description,parameters,status',
				'ref_table'=>$this->_target_model->table,
				'ref_id'=>$raw_row['id'],
				'locale'=>$this->_available_locales(),
			);
			if(isset($raw_row['is_live'])) $loc_options['is_live'] = $raw_row['is_live'];

			$row['loc'] = $this->text_locale_model->find($loc_options);
			$cur_locale = $this->lang->locale();

			if(isset($row['loc'][$cur_locale])){
				$loc_data = $row['loc'][$cur_locale];

				if(!empty($loc_data['title'])){
					$row['loc_title'] = $loc_data['title'];
				}
				if(!empty($loc_data['description'])){
					$row['loc_description'] = $loc_data['description'];
				}
				if(!empty($loc_data['content'])){
					$row['loc_content'] = $loc_data['content'];
				}
				if(!empty($loc_data['parameters'])){
					$row['loc_parameters'] = $loc_data['parameters'];
				}
			}
		}

		if(in_array('title',$this->_target_model->fields)){
			$row['title'] = isset($raw_row['title']) ? $raw_row['title'] : '';
			
			if(isset($raw_row['loc_title'])){
				$row['title'] = $raw_row['loc_title'];
			}
		}
		
		if(in_array('description',$this->_target_model->fields)){
			$row['description'] = isset($raw_row['description']) ? $raw_row['description'] : '';

			if(isset($raw_row['loc_description'])){
				$row['description'] = $raw_row['loc_description'];
			}
			$row['description_short'] = $this->_shorten_text($row['description']);
		}

		if(in_array('content',$this->_target_model->fields)){
			$row['content'] = isset($raw_row['content']) ? $raw_row['content'] : '';

			if(isset($raw_row['loc_content'])){
				$row['content'] = $raw_row['loc_content'];
			}

			$row['content_short'] = $this->_shorten_text(strip_tags($row['content']));
		}

		$row['status'] = isset($raw_row['status']) ? $raw_row['status'] : '';
		
		if(!empty($row['status'])){
			$row['status_str'] = lang('status_'.$raw_row['status']);
		}
		if(in_array('publish_date',$this->_target_model->fields)){
			$row['published'] = '';
			if(!empty($raw_row['publish_date'])){
				$row['published'] = $raw_row['publish_date'];
				$row['published_ts'] = strtotime($raw_row['publish_date']);
			}
		}
		if(in_array('create_date',$this->_target_model->fields)){
			$row['created'] = '';
			if(!empty($raw_row['create_date'])){
				$row['created'] = $raw_row['create_date'];
				$row['created_ts'] = strtotime($raw_row['create_date']);
			}
		}
		if(in_array('modify_date',$this->_target_model->fields)){
			$row['modified'] = '';
			if(!empty($raw_row['modify_date'])){
				$row['modified'] = $raw_row['modify_date'];
				$row['modified_ts'] = strtotime($raw_row['modify_date']);
			}
		}

		$row['_mapping'] = $raw_row['_mapping'];

		return $row;
	}

	function _clear_cache($raw_row){
		if(!empty($this->cache_prefix)){
			$cache_prefix = $this->cache_prefix;
			cache_remove($cache_prefix.'/'.$raw_row['id'].'/*');
			cache_remove($cache_prefix.'/'.$raw_row['id']);
			if(!empty($raw_row['_mapping'])){
				cache_remove($cache_prefix.'/'.$raw_row['_mapping'].'/*');
				cache_remove($cache_prefix.'/'.$raw_row['_mapping']);
			}
		}
	}

	function _segment_at($offset=0){
		$offset = $this->deep + 2 + $offset;
		return $this->uri->segment($offset);
	}
	
	function _remap(){
		
		$s1 = $this->_segment_at(0);
		$s2 = $this->_segment_at(1);
		$s3 = $this->_segment_at(2);
		$s4 = $this->_segment_at(3);

		if( $this->_mapping_action($s1,$s2,$s3,$s4) ){
			return ;
		}

		if( $this->_is_record_id($s1) ){
			if( $this->_record_action($s1, $s2,$s3, $s4) ){
				return;
			}
		}
		
		return $this->_show_404('route_not_matched');
	}

	function _mapping_action($s1,$s2=NULL,$s3=NULL,$s4=NULL){
		
		if(in_array($s1, array('','index','selector'))){
			$this->action = $s1;
			$this->index();
			return TRUE;
		}
		if(in_array($s1, array('search'))){
			$this->action = $s1;
			$this->search();
			return TRUE;
		}
		if(in_array($s1, array('batch'))){
			$this->action = $s1;
			$this->batch($s2);
			return TRUE;
		}
		if(in_array($s1, array('save'))){
			$this->action = $s1;
			$this->save();
			return TRUE;
		}
		if(in_array($s1, array('remove','delete'))){
			$this->action = $s1;
			$this->delete();
			return TRUE;
		}
		if(in_array($s1, array('add'))){
			$this->action = $s1;
			$this->editor();
			return TRUE;
		}
		return FALSE;
	}

	function _is_record_id($str){
		$id_pattern = '/^[0-9]+$/';
		if($this->_target_model->use_guid){
			$id_pattern = '/^([a-zA-Z0-9-]+)$/';
		}

		return preg_match($id_pattern,$str);
	}

	function _record_action($id, $action=false, $action_id=NULL, $subaction=NULL){
		if(!$action || empty($action) ){
			$action = 'view';
		}

		if($action == 'edit'){
			$this->action = $action;
			$this->editor($id);
			return TRUE;
		}

		if( $action == 'view'){
			$this->action = $action;
			$this->view($id);
			return TRUE;
		}

		return FALSE;
	}
	
	function index(){
		
		if( $this->_restrict()){
			return;
		}

		$vals = $this->_get_default_vals('index');
		
		if($this->uri->is_extension('js'))
			return $this->_render($this->view_prefix.$this->view_scope.'/'.$this->view_type.'_index.js',$vals);
		
		$this->_render($this->view_prefix.$this->view_scope.'/'.$this->view_type.'_index',$vals);
	}
	
	// default record's view
	function view($record_id=false){

		$method = $this->input->request_method();

		$record = NULL;
		if(!empty($record_id)){
			$query_opts = $this->_select_options('editor', array('id'=>$record_id));
			$record = $this->_target_model->read($query_opts);
			if(empty($record['id'])){
				return $this->_show_404('record_not_found');
			}
			if($this->uri->is_extension($this->supported_data_extension) && strtoupper($method) == 'GET'){
				return $this->_api( $this->_mapping_row($record) );
			}
		}

		if($this->has_record_view){

			$vals = $this->_get_default_vals('view');
			$vals['record'] = $record;
			$vals['record_id'] = $record_id;

			if($this->uri->is_extension('js'))
				return $this->_render($this->view_prefix.$this->view_scope.'/'.$this->view_type.'_view.js',$vals);
			
			return $this->_render($this->view_prefix.$this->view_scope.'/'.$this->view_type.'_view',$vals);

		}else{
			return redirect($this->endpoint_path_prefix.'/'.$record_id.'/edit');
		}

		return $this->_show_404('no_view');
	}
	
	function editor($record_id=false){
		
		if( $this->_restrict()){
			return;
		}
		
		$record = NULL;

		$vals = NULL;
		
		if(!empty($record_id)){
			$query_opts = $this->_select_options('editor', array('id'=>$record_id));
			$record = $this->_target_model->read($query_opts);
			if(empty($record['id'])){
				return $this->_show_404('record_not_found');
			}
			$vals = $this->_get_default_vals('edit', compact('record','record_id'));
			$vals['id'] = $record_id;
			$vals['record_id'] = $record_id;
			$vals['record'] = $this->_mapping_row($record);
			if(!isset($vals['data']))
				$vals['data'] = $record;
		}else{
			$vals = $this->_get_default_vals('add');
			$vals['id'] = NULL;
			$vals['record_id'] = NULL;
			$vals['record'] = NULL;
			if(!isset($vals['data']))
				$vals['data'] = $this->_target_model->new_default_values();
		}

		if($this->localized){
			// localized content
			$vals['loc'] = array();
			if(!empty($record)){
				
				$vals['loc'] = $this->text_locale_model->find(array('ref_table'=>$this->_target_model->table,'ref_id'=>$record_id,'is_live'=>'0','_field_based'=>'locale'));

				$loc_fields = $this->locale_fields;

				foreach($this->lang->get_available_locale_keys() as $loc_key ){
					if(!isset($vals['loc'][$loc_key])){
						$vals['loc'][$loc_key]=array();
						$vals['loc'][$loc_key]['locale'] = $loc_key;

						foreach($loc_fields as $loc_field){
							if(isset($record[ $loc_field ])){
								$vals['loc'][$loc_key][$loc_field] = $record[$loc_field];
							}
						}
					}
				}
			}
		}
		
		if($this->uri->is_extension('js'))
			return $this->_render($this->view_prefix.$this->view_scope.'/'.$this->view_type.'_editor.js',$vals);
		
		$this->_render($this->view_prefix.$this->view_scope.'/'.$this->view_type.'_editor',$vals);
	}

	function _search_options($options=false){

		if(!$options) $options = array();

		$direction = $this->sorting_direction;
		$sort = $this->sorting_fields[0];

		$start = 0;
		$limit = 50;
		
		if($this->input->get('direction')!==false){
			$direction = $this->input->get('direction');
			if(strtolower($direction) != 'desc') $direction = 'asc';
		}
		if($this->input->get('sort')!==false){
			$sort = $this->input->get('sort');

			if(!in_array($sort,$this->sorting_fields)) $sort = $this->sorting_fields[0];
		}
		
		if($this->input->get('offset')!==false){
			$start = $this->input->get('offset');
		}
		if($this->input->get('limit')!==false){
			$limit = $this->input->get('limit');
		}
			
		if($this->input->get('page')!==FALSE){
			$start = ($this->input->get('page') - 1 ) * $limit;
		}
		
		if($this->input->get('q')!=false && $this->input->get('q')!=''){
			$options['_keyword'] = $this->input->get('q');
			$options['_keyword_fields'] = $this->keyword_fields;
		}
		
		if($this->home_related){
			if($this->input->get('home_visible')!=''){
				$options['home_visible'] = $this->input->get('home_visible');
			}
		}
		
		if($start<0) $start = 0;
		if($limit < 5) $limit = 10;
		elseif($limit%5 != 0) $limit = 10; 
		
		
		$options['_order_by'] = array($sort=>$direction);
		if( in_array('create_date',$this->_target_model->fields))
			$options['_order_by'][ $this->_target_model->table.'.create_date' ] = $direction;

		if($this->localized){
			$options['_with_text'] = $this->lang->locale();
		}

		$options['_paging_start'] = $start;
		$options['_paging_limit'] = $limit;

		return $options;
	}

	function _select_options($action='default',$options=array()){
		return $options;
	}
	
	function search(){
		if($this->_restrict()){
			return ;
		}
		
		if(!$this->uri->is_extension($this->supported_data_extension)){ 
			return $this->_show_404('extension_not_allowed');
		}
		
		$options = $this->_search_options();
		
		$result = $this->_target_model->find_paged($options['_paging_start'],$options['_paging_limit'],$options,false);
		

		$vals = $this->_get_default_vals('search');
		if(isset($result['data'])){
			
			foreach($result['data'] as $idx => $row){
				$new_row = $this->_mapping_row($row);
				$new_row['_index'] = $result['index_from']  + $idx;
				$vals['data'][] = $new_row;
			}
			
			$vals['paging']['offset'] = $result['index_from'];
			$vals['paging']['total'] = $result['total_record'];
			$vals['paging']['limit'] = $result['limit'];
			$vals['paging']['page'] = $result['page'];
			$vals['paging']['total_page'] = $result['total_page'];
		}
		return $this->_api($vals);
	}

	function batch($action=''){
		if($this->_restrict()){
			return ;
		}
		
		$ids = $this->input->get_post('ids');
		$ids = explode(",", trim($ids));
		if(!is_array($ids)){
			return $this->_error(ERROR_INVALID_DATA, lang(ERROR_INVALID_DATA_MSG));
		}
		
		$query_opts = $this->_select_options('batch', array('id'=>$ids));

		$records = $this->_target_model->find($query_opts);
		if(is_array($records) && count($records)>0){
			foreach($records as $idx => $record){
				
				$result[ $record['id'] ] = $this->_batch_action($action, $record);
			}
			
			return $this->_api(array('result'=>$result,'data'=>$ids));
		}else{
			return $this->_error(ERROR_NO_RECORD_LOADED,lang(ERROR_NO_RECORD_LOADED_MSG));
		}
	}

	function _batch_action($action, $record){
		$query_opts = $this->_select_options($action, array('id'=>$record['id']));
				
		if($action == 'publish' || $action == 'status-publish' || $action == 'status-enable'){
			$this->_target_model->save(array('status'=>1), $query_opts);

			return TRUE;
		}elseif($action == 'draft' || $action == 'status-draft' || $action == 'status-disable'){
			$this->_target_model->save(array('status'=>0), $query_opts);

			return TRUE;
		}

		return FALSE;
	}
	
	function save($id=false){
		
		if($this->_restrict()){
			return ;
		}

		$editor_info =$this->_get_editor_info();
		
		$vals = $this->_get_default_vals('save');
		$success = true;
		
		if(!$id) $id = $this->input->get_post('id');
		
		$query_opts = $this->_select_options('save', array('id'=>$id));

		$record = $old_record = NULL;
		if(!empty($id)){
			$old_record = $record =$this->_target_model->read($query_opts) ;
			if(!isset($record['id']) || $record['id'] != $id){
				return $this->_show_404('record_id_not_matched');
			}
		}

		$action = !$id ? 'add' : 'edit';
		$data = array();

		$success = $this->_before_save($action, $id, $data);
		
		if($success != FALSE)
			$success = TRUE;

		if($success){

			$validate = array();
			if(!$this->_validate_save($query_opts, $old_record, $data, $validate)) {
				return $this->_error(ERROR_RECORD_VALIDATION, lang(ERROR_RECORD_VALIDATION_MSG),'200', compact('validate'));
			}

			//$locale = $this->lang->locale();

			if($this->localized){
				// prepare data for localized content
				$all_loc_data = $this->input->post('loc');
				$default_locale = $this->input->post('default_locale');
				$locale = $this->lang->locale();

				// only this fields will be handled for localized
				$locale_fields = $this->locale_fields;

				if(empty($default_locale)) $data['default_locale'] = $default_locale = $this->lang->locale();

				$loc_data = isset($all_loc_data[$default_locale]) ? $all_loc_data[$default_locale] : NULL;

				$sql_loc_data = array();
				foreach($locale_fields as $idx => $field_name){
					if(isset($loc_data[$field_name]))
						$data [$field_name] = $loc_data[$field_name];
				}

				if(empty($all_loc_data)){
					foreach($this->lang->get_available_locale_keys() as $loc_code){
						$loc_data = array();
						foreach($locale_fields as $idx => $field_name){
							if(isset( $data[$field_name]))
							$loc_data[$field_name] = $data[$field_name];
						}
						$all_loc_data[$loc_code] = $loc_data;
					}
				}
			}

			if(!$id){
				$result = $this->_target_model->save($data,NULL, $editor_info);
				$query_opts = $this->_select_options('save', array('id'=>$result['id']));
				$id = $result['id'];
			}else{
				unset($data['id']);
				unset($data['create_date']);
				unset($data['create_by']);
				unset($data['create_by_id']);
				$result = $this->_target_model->save($data,$query_opts, $editor_info);
			}

			$record = $this->_target_model->read($query_opts);

			// localized part
			if($this->localized){

				// required helper and models
				$this->load->helper('localized');

				foreach($this->lang->get_available_locale_keys() as $loc_code){
					$loc_data = isset($all_loc_data[$loc_code]) ? $all_loc_data[$loc_code] : NULL;

					// skip it if no data for this locale
					if(empty($loc_data)) continue;
					localized_save($this->_target_model->table, $id, $loc_code, $loc_data,'0',$editor_info);
				}
			}

			
			$vals['id'] = $id;
			$vals['method'] =$action;
			$vals['_mapping'] = $record['_mapping'];

			$this->_after_save($action, $id, $old_record, $record, $vals);
			$this->_clear_cache($record);
		}
		
		if($this->uri->is_extension('')){
			redirect($this->view_prefix.$this->view_scope.'/'.$this->view_type.'/'.$id);
			return;
		}
		return $this->_api($vals);
	}

	function _before_save($action, $record_id, &$data=false){

		$defvals = $this->_target_model->new_default_values();

		foreach($defvals as $field=>$val){

			// Ignore fields if the default values does not allowed for save action
			if(!empty($this->editable_fields)){
				if(!in_array($field, $this->editable_fields))
					continue;
			}
			$post_value = $this->input->post($field);

			if(!isset($data[$field]))
				$data[$field] = $val;
			if(isset($record[$field])) 
				$data[$field] = $record[$field];
			if(!empty($post_value )) 
				$data[$field] = $post_value ;
		}
		
		if(isset($data['publish_date'])){
			if((substr($data['publish_date'],0,4) == '0000' || empty($data['publish_date'])) && $data['status'] == '1'){
				$data['publish_date'] = date('Y-m-d H:i:s');
			}
		}

			

		return TRUE;
	}

	function _validate_save($query_options, $old_record, $data, &$validate = NULL){


		$_validate = $this->_target_model->validate($data, $query_options);

		// clone result
		foreach($_validate as $key => $val) $validate[$key] = $val;
		
		if(!$_validate['success']){
			return FALSE;
		}
		return TRUE;
	}

	function _after_save($action, $id, $old_record, $record, &$vals = false){

		if($this->home_related){
			// update home visible content
			if($record['home_visible'] == '1'){
				$home_content_row = $this->home_content_model-> read( array(
					'ref_scope' => $this->_target_model->table,
					'ref_id'=>$record['id'],
				));
				if(!isset($home_content_row['id'])){
					$this->home_content_model->save(array(
						'ref_scope'=> $this->_target_model->table,
						'ref_id'=>$record['id'],
						'ref_slug'=>isset($record['slug']) ? $record['slug'] : NULL,
						'cover_id'=>isset($record['cover_id']) ? $record['cover_id'] : NULL,
						'title'=>$record['title'],
						'description'=>$record['description'],
						'publish_date'=>$record['publish_date'],
						'status'=>$record['status'],
					));
				}else{
					$this->home_content_model->save(array(
						'ref_slug'=>isset($record['slug']) ? $record['slug'] : NULL,
						'cover_id'=>isset($record['cover_id']) ? $record['cover_id'] : NULL,
						'title'=>$record['title'],
						'description'=>$record['description'],
						'publish_date'=>$record['publish_date'],
						'status'=>$record['status'],
					),array(
						'id'=>$home_content_row['id'],
					));
				}
			}else{
				$this->home_content_model-> delete( array(
					'ref_scope' => $this->_target_model->table,
					'ref_id'=>$record['id'],
				));
			}
			cache_remove('home_contents');
		}
	}

	function delete(){
		if($this->_restrict()){
			return ;
		}
		
		$ids = $this->input->post('ids');
		$ids = explode(",", trim($ids));
		if(!is_array($ids)){
			return $this->_error(ERROR_INVALID_DATA, lang(ERROR_INVALID_DATA_MSG));
		}
		
		$query_opts = $this->_select_options('delete', array('id'=>$ids));

		$records = $this->_target_model->find($query_opts);
		if(is_array($records) && count($records)>0){
			foreach($records as $idx => $row){
				
				if($this->localized){
					// remove multiple language record
					$this->text_locale_model-> delete(array('ref_scope'=>$this->_target_model->table,'ref_id'=>$row['id']));
				}

				$query_opts = $this->_select_options('delete', array('id'=>$row['id']));
				$this->_target_model->delete($query_opts);

				$this->_after_delete($row['id'], $row);

				$this->_clear_cache($row);
			}
			
			
			return $this->_api(array('data'=>$ids));
		}else{
			return $this->_error(ERROR_NO_RECORD_LOADED, lang(ERROR_NO_RECORD_LOADED_MSG));
		}
	}

	function _after_delete($id, $record){

		if($this->home_related){
			$this->home_content_model-> delete( array(
				'ref_scope' => $this->_target_model->table,
				'ref_id'=>$record['id'],
			));
		}
	}

	function _render($view, $vals = false, $layout = false, $theme = false){

		if(!$vals) $vals = array();
		$vals['sorting_fields'] = $this->sorting_fields;
		$vals['sorting_direction'] = $this->sorting_direction;
		$vals['keyword_fields'] = $this->keyword_fields;

		// if passed data does not contain path prefix...
		if(empty($vals['view_path_prefix'])) 
			$vals['view_path_prefix'] = $this->view_path_prefix;

		// if controller does not contain path prefix, combined by configured version
		if(empty($vals['view_path_prefix'])) 
			$vals['view_path_prefix'] = $this->view_prefix.$this->view_scope.'/'.$this->view_type;

		// if passed data does not contain path prefix...
		if(empty($vals['endpoint_path_prefix'])) 
			$vals['endpoint_path_prefix'] = $this->endpoint_path_prefix;

		// if controller does not contain path prefix, combined by configured version
		if(empty($vals['endpoint_path_prefix'])) 
			$vals['endpoint_path_prefix'] = $this->view_prefix.$this->view_scope.'/'.$this->view_type;
		$vals['endpoint_url_prefix'] = site_url($vals['endpoint_path_prefix']);
		
		$vals['view_prefix'] = $this->view_prefix;
		$vals['view_scope'] = $this->view_scope;
		$vals['view_type'] = $this->view_type;

		if(!isset($vals['page_header']))
			$vals['page_header'] = lang($this->page_header);

		return parent::_render($view, $vals, $layout, $theme);
	}
}