<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class HC_Loader extends CI_Loader
{

	function __construct($cfg=false){

		parent::__construct($cfg);

		log_message('debug', "HC_Loader Class Initialized");
	}

	function widget($view, $vals=NULL,$return = FALSE){
		if(!is_array($vals)) $vals = array();

		$_ci_CI =& get_instance();
		
		$theme = $this->config->item('theme');

		$req_ext = EXT;

		// explode file extension
		if(substr($view,- strlen(EXT)) == EXT) {
			$segs = explode('/', $view);
			$file = $segs [ count($segs) - 1];
			$_info = explode('.',2);
			$req_ext = $_info[1];
		}

		$view_path = NULL;
		if (empty($view_path)) {
			$widget_path = 'themes/' . $theme . '/widgets/';
			$view_path = $widget_path . $view . $req_ext;
			if (!file_exists(APPPATH . 'views/' . $view_path)) {
				$view_path = NULL;
			}
		}
		if (empty($view_path)) {
			$widget_path ='widgets/';
			$view_path = $widget_path . $view . $req_ext;
			if (!file_exists(APPPATH . 'views/' . $view_path)) {
				$view_path = NULL;
			}
		}
		if(empty($view_path)){
			show_error('Widget "'.$view.'" does not exist.');
			return;
		}

		$this->load->helper('string');
		$_hash = $element_id = random_string('alnum',16);

		if(!empty($vals) && is_array($vals))
			extract($vals);

		/*
		 * Buffer the output
		 *
		 * We buffer the output for two reasons:
		 * 1. Speed. You get a significant speed boost.
		 * 2. So that the final rendered template can be
		 * post-processed by the output class.  Why do we
		 * need post processing?  For one thing, in order to
		 * show the elapsed page load time.  Unless we
		 * can intercept the content right before it's sent to
		 * the browser and then stop the timer it won't be accurate.
		 */
		ob_start();

		//print '<!-- WIDGET: '. $view.', element:'.$element_id.' widget_path:'. $widget_path .' -->'."\r\n";
		include APPPATH . 'views/' .$view_path;
		//print "\r\n<!-- /WIDGET -->\r\n";
		// Return the file data if requested
		if ($return === TRUE)
		{
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}

		/*
		 * Flush the buffer... or buff the flusher?
		 *
		 * In order to permit views to be nested within
		 * other views, we need to flush the content back out whenever
		 * we are beyond the first level of output buffering so that
		 * it can be seen and included properly by the first included
		 * template and any subsequent ones. Oy!
		 *
		 */
		if (ob_get_level() > $this->_ci_ob_level + 1)
		{
			ob_end_flush();
		}
		else
		{
			$_ci_CI->output->append_output(ob_get_contents());
			@ob_end_clean();
		}

	}
}