<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ACP_Controller extends HC_Controller
{

	var $user_type = 'admin';
	var $config_file = 'acp';
	var $auth_config = array(
			'table'			=>'admins',
			'path'			=>array(
				'login'		=>'auth/signin',
			),
			'field'			=>array(
				'loginkey'	=>'login_name',
				'password'  =>'login_pass',
			),
			'activiate'		=>'admin',
			'encrypt_config_key'=>'acp_encryption_key',
		);
	var $acl_config = array(
		'tables'=>array(

			'users'=>'admins',
			'roles'=>'admin_roles',
			'users_roles'=>'admins_roles',
			'users_permissions'=>'admins_permissions',
			'roles_permissions'=>'admin_roles_permissions',
			'permissions'=>'admin_permissions',
		),
		'fields'=>array(

			'created'=>'create_date',
			'user_id'=>'admin_id',
			'perm_id'=>'permission_id',
			),
	);

	var $auth_user_model = 'admin_model';

	function _get_editor_info(){
		return array('type'=>'admin', 'id'=>$this->acp_auth->get_id());
	}
	
	function __construct(){
		parent::__construct();
		
		// Feature: Language
		$sys_lang = $this->config->item('language');

		if( isset($this->session) && $this->session->userdata('language')!=NULL){
			$sys_lang =  $this->session->userdata('language');
		}
		
		$new_sys_lang = NULL;
		if($this->input->get('sys-lang')!=NULL){
			$new_sys_lang = $this->input->get('sys-lang');
		}
		
		if(!empty($new_sys_lang) && $new_sys_lang != $sys_lang){
			if($this->lang->is_supported_locale($new_sys_lang)){
				$sys_lang = $new_sys_lang;
				$this->config->set_item('language', $new_sys_lang);
				$this->session->set_userdata('language', $new_sys_lang);
			}
		}else{
			$this->config->set_item('language', $sys_lang);
		}
		
		$this -> load -> helper('language');

		$this->lang->load('common',$sys_lang);
		$this->lang->load('admin',$sys_lang);
		
		$this->init_auth();

		$this->init_menu();

	}

	protected function init_auth(){

		// Feature: Authentication
		$this->load->config($this->config_file);

		$this->load->library('LMS_Auth',$this->auth_config,'acp_auth');
		
		// Feature: Access Control List
		$this->load->library('acl',$this->acl_config);

		$this->load->model($this->auth_user_model, '_auth_user_model');
		$this->auth_user_model = $this->_auth_user_model;

		if ($this->acp_auth->is_login()) {
			
			$account = $this->auth_user_model->read(array('id' => $this->acp_auth->get_id()));

			if (isset($account['id'])) {
				$this->config->set_item('account', $account);
				$this->config->set_item('account_id', $account['id']);
			}

			if (isset($account['login_name'])) {
				$this->config->set_item('account_name', $account['login_name']);
			}

			if (isset($account['email'])) {
				$this->config->set_item('account_email', $account['email']);
			}
		}
	}

	function init_menu(){

		$this->load->config('ph');
		$sections = $this->config->item('ph_sections');

		if (is_array($sections)) {
			$layout_menu = $this->config->item('layout_menu');
			foreach ($sections as $section_name => $section_detail) {

				if(isset($section_detail['acp_menu_enabled']) && $section_detail['acp_menu_enabled'] == false) continue;

				$counter = 0;

				$_category_enabled = isset($section_detail['category_enabled']) && $section_detail['category_enabled'] && (!isset($section_detail['category_acp_menu']) || $section_detail['category_acp_menu']);
				$_tag_enabled = isset($section_detail['tag_enabled']) && $section_detail['tag_enabled'] && (!isset($section_detail['tag_acp_menu']) || $section_detail['tag_acp_menu']);
				$_has_relation = $_category_enabled || $_tag_enabled;

				if($_has_relation){
					$subitems   = array();
					$subitems[] = array(
						'tree' => array($section_name, 'post'),
						'url'      => 's/' . $section_name . '/post',
						'text'     => 'post_heading',
						'icon'     => isset($section_detail['post_icon']) ? $section_detail['post_icon'] : 'fa fa-file',
						/*
						'subitems' => array(
							array(
								'tree' => array($section_name, 'post'),
								'url'  => 's/' . $section_name . '/post',
								'text'=>'button_catalog',
								'icon' => 'fa fa-filter',
							),
							array(
								'tree' => array($section_name, 'post'),
								'url'  => 's/' . $section_name . '/post/add',
								'text' => 'button_add',
								'icon' => 'fa fa-plus',
							),
						)
						//*/
					);
					if ($_category_enabled) {
						$counter++;
						$subitems[] = array(
							'tree' => array($section_name, 'category'),
							'url'      => 's/' . $section_name . '/category',
							'text'     => 'category_heading',
							'icon'     => isset($section_detail['category_icon']) ? $section_detail['category_icon'] : 'fa fa-chain',

						/*
							'subitems' => array(
								array(
									'url'  => 's/' . $section_name . '/category',
								'text'=>'button_catalog',
									'icon' => 'fa fa-filter',
								),
								array(
									'url'  => 's/' . $section_name . '/category/add',
									'text' => 'button_add',
									'icon' => 'fa fa-plus',
								),
							)
							//*/
						);
					}
					if ($_tag_enabled) {
						$counter++;
						$subitems[] = array(
							'tree' => array($section_name, 'tag'),
							'url'      => 's/' . $section_name . '/tag',
							'text'     => 'tag_heading',
							'icon'     => isset($section_detail['tag_icon']) ? $section_detail['tag_icon'] : 'fa fa-tags',
						/*
							'subitems' => array(
								array(
									'tree' => array($section_name, 'tag'),
									'url'  => 's/' . $section_name . '/tag',
								'text'=>'button_catalog',
									'icon' => 'fa fa-filter',
								),
								array(
									'tree' => array($section_name, 'tag'),
									'url'  => 's/' . $section_name . '/tag/add',
									'text' => 'button_add',
									'icon' => 'fa fa-plus',
								),
							)
						//*/
						);

					}
					$cfg = array(
						'tree' => array($section_name),
						'url'      => 's/' . $section_name,
						'text'     => $section_name . '_heading',
						'icon'     => isset($section_detail['icon']) ? $section_detail['icon'] : 'fa fa-file-text',
						'subitems' => $subitems,
					);
				}else{
					$cfg = array(
						'tree' => array($section_name),
						'url'      => 's/' . $section_name . '/post',
						'text'     => $section_name . '_heading',
						'icon'     => isset($section_detail['icon']) ? $section_detail['icon'] : 'fa fa-file-text',
						/*
						'subitems' => array(
							array(
								'tree' => array($section_name, 'post'),
								'url'  => 's/' . $section_name . '/post',
								'text'=>'button_catalog',
								'icon' => 'fa fa-filter',
							),
							array(
								'tree' => array($section_name, 'post'),
								'url'  => 's/' . $section_name . '/post/add',
								'text' => 'button_add',
								'icon' => 'fa fa-plus',
							),
						)
					//*/
					);
				}

				if (isset($section_detail['perms'])) {
					$cfg['perms'] = $section_detail['perms'];
				}
				$layout_menu[] = $cfg;
			}
			$this->config->set_item('layout_menu', $layout_menu);
		}
	}
	
	function _restrict($scope = NULL,$redirect=true){
		
		if($redirect && $this->acp_auth->restrict())
			return true;
		if(!$redirect && !$this->acp_auth->is_login())
			return true;
		
		$this->acl->set_user_id($this->acp_auth->get_id());
		
		if(!empty($scope)){
			if(!$this->acl->has_permission($scope)){
				$this->_permission_denied($scope);
				return true;
			}
		}
		return false;
	}
	
	function _render($view, $vals=false, $layout=false, $theme =false){


		if ($this->input->get('dialog') == 'yes' || $this->input->get('dialog') == 'true' || $this->input->get('dialog') == '1') {
			$vals['is_dialog'] = true;
			$this->render->addClass('body', 'dialog');
			if (!$layout) {
				$layout = 'dialog';
			}
		}

		return parent::_render($view, $vals, $layout, $theme);
	}

}
