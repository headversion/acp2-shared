<?php


function acp_class_loader($class_name) {

	$file_path = PACKAGE_DIR.'/acp2-shared/core/'.$class_name .'.php';
	if(file_exists($file_path)){
	    require_once $file_path;
	    return ;
	}
	//die('test:'.$file_path);

	$file_path = PACKAGE_DIR.'/acp2-shared/core/'.strtolower($class_name) .'.php';
	if(file_exists($file_path)){
	    require_once $file_path;
	    return ;
	}


}

function acp_loader_init(){
	spl_autoload_register('acp_class_loader');
}