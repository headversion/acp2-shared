<?php

class PictureHelper
{
	static $default_folder = 'picture';

	static $enable_logging = FALSE;

	
	function resize($src_filename, $dst_filename, $options=false){
		
		ini_set('memory_limit','128M');
		
		if(!file_exists($src_filename)){
			log_message('error','PictureHelper::resize: file not found: '.$src_filename.' ');
		
			return FALSE;	
		}
		if(PictureHelper::$enable_logging){
			log_message('debug','PictureHelper::resize: for '.$src_filename.' to '.json_encode($options));
		}
		
		$info = @getimagesize($src_filename);
		if(!isset($info[0]) || !isset($info[1])){
			log_message('error','PictureHelper::resize: no size info for '.$src_filename.' to '.json_encode($options));
			return FALSE;
		}
		
		$src_w = $info[0];
		$src_h = $info[1];
		
		$has_tar_w = false;
		$has_tar_h = false;
		$tar_w = $src_w;
		$tar_h = $src_h;
		
		if(isset($options ['width'])){
			$tar_w = $options ['width'];
			$has_tar_w = true;
		}
		if(isset($options ['height'])){
			$tar_h = $options ['height'];
			$has_tar_h = true;
		}
		
		$CI = &get_instance();
		$CI->load->library('image_lib');
		
		
		$quality = 100;
		if(isset($options['quality'])){
			$quality = $options['quality'];
		}
		
		if(isset($options['crop_x']) && isset($options['crop_y']) && isset($options['crop_width']) && isset($options['crop_height'])){
			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, [crop] before crop size ('.$info[0].'w x '.$info[1].'h)');
			}
			$CI->image_lib->clear();
			
			$cfg = array(
				'source_image'	=> $src_filename,
				'new_image'		=> $dst_filename,
				'maintain_ratio'=> FALSE,
				'x_axis'		=> $options['crop_x'],
				'y_axis'		=> $options['crop_y'],
				'width'			=> $options['crop_width'],
				'height'		=> $options['crop_height'],
				'quality'		=> $quality,
			);

			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, [crop] cfg ('.json_encode($cfg).')');
			}
			$CI->image_lib->initialize($cfg);
			
			if($CI->image_lib->crop()){
				if(file_exists($dst_filename)){
					$info = @getimagesize($dst_filename);
					if(PictureHelper::$enable_logging){
						log_message('debug','PictureHelper::resize, [crop] after crop size ('.$info[0].'w x '.$info[1].'h)');
					}
					
					if($has_tar_w && $has_tar_h){
						
						$CI->image_lib->clear();
						$cfg = array(
							'source_image'	=> $dst_filename,
							'new_image'		=> $dst_filename,
							'width'			=> $tar_w,
							'height'		=> $tar_h,
							'maintain_ratio'=> FALSE,
							'quality'		=> $quality,
						);
						$CI->image_lib->initialize($cfg);
						
						if(!$CI->image_lib->resize()){
							log_message('error','PictureHelper::resize, image_lib cannot perform with this setting:'.json_encode($cfg));
							return FALSE;
						}
						$info = @getimagesize($dst_filename);

						if(PictureHelper::$enable_logging){
							log_message('debug','PictureHelper::resize, [crop] after resize ('.$info[0].'w x '.$info[1].'h)');
						}
					}
				}else{
					log_message('error','PictureHelper::resize, [crop] file does not created after crop');
				}
				
				return TRUE;	
			}
			log_message('error','PictureHelper::resize, [crop] cannot crop image by giving crop area setting');
			
		
			return FALSE;
		}
		
		
		$crop_x = 0;
		$crop_y = 0;
		
		$type = isset($options ['type']) ? ($options ['type']) : '';
		$crop = isset($options ['crop']) && $options ['crop'] === TRUE ? $options ['crop'] : false;
		
		if($type =='fill'){
			
			// scale to fill the boundary
			$scale = 1 ;
			if( $has_tar_w ){
				$scale = $tar_w / $src_w;;
				if(PictureHelper::$enable_logging){
					log_message('debug','PictureHelper::resize, after scaled by width | scale='.$scale);
				}
				if($has_tar_h && $scale * $src_h < $tar_h){
					$scale = $tar_h / $src_h;
					if(PictureHelper::$enable_logging){
						log_message('debug','PictureHelper::resize, after scaled by height | scale='.$scale);
					}
				}
			}elseif( $has_tar_h ){
				$scale = $tar_h / $src_h;
				if(PictureHelper::$enable_logging){
					log_message('debug','PictureHelper::resize, after scaled by height | scale='.$scale);
				}
			}
			if($scale > 1) $scale = 1;
		}else{
			
			// scale to fit the boundary
			$scale = 1;
			if($has_tar_w && $scale * $src_w > $tar_w)
				$scale = $tar_w / $src_w;

			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, after scaled by width | scale='.$scale);
			}
			if($has_tar_h && $scale * $src_h > $tar_h){
				$scale = $tar_h / $src_h;
			}
			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, after scaled by height | scale='.$scale);
			}
			if($scale > 1) $scale = 1;
		}
			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, after scaled by fixed max scale | scale='.$scale);
			}
		
		$to_w = ceil($src_w * $scale);
		$to_h = ceil($src_h * $scale);
		$to_x = floor($tar_w * 0.5 - $to_w  * 0.5);
		$to_y = floor($tar_h * 0.5 - $to_h  * 0.5);
		
		if(PictureHelper::$enable_logging){
			log_message('debug','PictureHelper::resize, information of '.basename($src_filename).': '.json_encode(compact(
				'crop','type','to_x','to_y','to_w','to_h','scale','src_w','src_h','has_tar_w','tar_w','has_tar_h','tar_h'
				)));
		}
	
		if($crop ){
			
			
			
			// step 1: scale image to target size
			$CI->image_lib->clear();
			$cfg = array(
				'source_image'	=> $src_filename,
				'new_image'		=> $dst_filename,
				'width'			=> $to_w,
				'height'		=> $to_h,
				'maintain_ratio'=> FALSE,
				'quality'		=> $quality,
			);
			$CI->image_lib->initialize($cfg);
			
			if(!$CI->image_lib->resize()){
				log_message('error','PictureHelper::resize, image_lib cannot perform with this setting:'.json_encode($cfg));
				return FALSE;
			}
			
			if(!file_exists($dst_filename)){
				log_message('error','PictureHelper::resize, file does not created after resize');
				return FALSE;
			}
			$info = @getimagesize($dst_filename);


			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, after resize ('.$info[0].'w x '.$info[1].'h)');
			}
			
			
			// step 2: crop in center
			$CI->image_lib->clear();
			$cfg = array(
				'source_image'	=> $dst_filename,
				'new_image'		=> $dst_filename,
				'x_axis'		=> $to_x < 0 ? - $to_x : 0,
				'y_axis'		=> $to_y < 0 ? - $to_y : 0,
				'width'			=> $to_w < $tar_w ? ceil($to_w) : $tar_w,
				'height'		=> $to_h < $tar_h ? ceil($to_h) : $tar_h,
				'maintain_ratio'=> FALSE,
				'quality'		=> $quality,
			);
			$CI->image_lib->initialize($cfg);
			
			if(!$CI->image_lib->crop()){
				log_message('error','PictureHelper::resize, image_lib cannot perform with this setting:'.json_encode($cfg));
				return FALSE;
			}
			$CI->image_lib->clear();
			
			if(!file_exists($dst_filename)){
				log_message('error','PictureHelper::resize, file does not created after crop');
				return FALSE;
			}
			$info = @getimagesize($dst_filename);


			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, after crop size ('.$info[0].'w x '.$info[1].'h)');
			}
			
			return TRUE;	
			
		}else{
				
			$CI->image_lib->clear();
			$cfg = array(
				'source_image'	=> $src_filename,
				'new_image'		=> $dst_filename,
				'width'			=> $to_w,
				'height'		=> $to_h,
				'maintain_ratio'=> FALSE,
			);
			$CI->image_lib->initialize($cfg);
			
			if(!$CI->image_lib->resize()){
				log_message('error','PictureHelper::resize, image_lib cannot perform with this setting:'.json_encode($cfg));
				return FALSE;
			}
			$CI->image_lib->clear();
			
			if(!file_exists($dst_filename)){
				log_message('error','PictureHelper::resize, file does not created after resize');
				return FALSE;
			}
			
   			
			$info = @getimagesize($dst_filename);

			if(PictureHelper::$enable_logging){
				log_message('debug','PictureHelper::resize, after resize ('.$info[0].'w x '.$info[1].'h)');
			}
			
			return TRUE;
	
		}
	}
	/*
	function subfolder($photo=false,$folder='photo'){
		
		//log_message('debug','PictureHelper::helper#start');
		$CI = &get_instance();
		
		if(!isset($photo['id']) && is_string($photo)){
			$CI->load->model('photo_model');
			
			log_message('debug','PictureHelper::subfolder, requesting:'.$photo);
			$photo = $CI->photo_model->read($photo);
		}
		if(!isset($photo['id'])){
			
			log_message('error','PictureHelper::subfolder, data not found');
			return NULL;
		}
		
		if(!is_writable(PUB_DIR.DS.$folder)){
			log_message('error','PictureHelper::subfolder, '.basename(PUB_DIR).'/'.$folder.' not writable when using ');
			return '';
		}
		
		$str = md5($photo['id']);
		
		$folder_vals = array();
		for($i=0; $i< strlen($str); $i++){
			if(!isset($folder_vals[$i%3])) $folder_vals[$i%3] = 0;
			$folder_vals[$i%3] += ord($str[$i]);
		}
		
		for($i=0; $i< count($folder_vals); $i++){
			$folder_vals[$i] = $folder_vals[$i]%36;
		}
		
		$prefix = '';
		
		$root = PUB_DIR.DS.$folder.'/';
		
		for($i=0;$i< count($folder_vals) ; $i++){
			$folder_char = $folder_vals[$i] < 10 ? $folder_vals[$i] : chr( 65 + $folder_vals[$i] - 10);
			
			$prefix .= $folder_char;
			$prefix .='/';
			
			if(!is_dir($root.$prefix))
				@mkdir($root.$prefix,0777);
		}
		
		return $prefix;
	}
//*/
	function get_name($file=false, $size_group=false, $size_name=false,$options=false)
	{
		
		$CI = &get_instance();
		
		
		$_pos = strrpos($file,'.');
		
		$file_name = substr($file,0,$_pos);
		$file_ext = $_pos >= 0 ? substr($file,$_pos):'';
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		

		// if the it given sizetype group name,
		// then we try to load the picture sizetypes group
		$CI->load->helper('data');
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		$CI->load->config('picture');
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		
		$_size_groups = $CI->config->item('picture_sizes');
		$_size_group = NULL;
		$size_info = NULL;
		
		if(is_string($size_group)){
			if(isset($_size_groups[$size_group])){
				$_size_group = $_size_groups[$size_group];
			}
		}elseif(is_array($size_group)){
			$_size_group = $size_group;
		}
		
		if(!$_size_group && isset($size_groups['default'])){
			// default group must always available
			$_size_group = $size_groups['default'];
		}
		
		if(is_string($size_name)){
			if(isset($_size_group[$size_name])){
				$size_info = $_size_group[$size_name];
			}
		}
		
		if(!$size_info && isset($_size_group['default'])){
			// default group must always available
			$size_info = $_size_group['default'];
		}
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		if(!isset($size_info) || empty($size_info)) {
			log_message('error','PictureHelper::make, empty sizetypes. config='.print_r(compact('_size_groups','size_name'),true));
			return NULL;
		}
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		$is_rebuild = isset($options['rebuild'] ) && $options['rebuild'] == true;
		$is_overwrite = isset($options['overwrite'] ) && $options['overwrite'] == true;
		$src_dir = isset($options['src'] ) ? $options['src'] : PRV_DATA_DIR.DS.PictureHelper::$default_folder;
		$dest_dir = isset($options['dest'] ) ? $options['dest'] : $src_dir;
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		
		$target_name = $file_name;
		if(isset($size_info['prefix'])) $target_name = $size_info['prefix']. $target_name;
		if(isset($size_info['suffix'])) $target_name = $target_name. $size_info['suffix'];
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		$dest_file = $target_name.strtolower($file_ext);;
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		if(substr($src_dir,-1,1) != DIRECTORY_SEPARATOR){
			$src_dir .= DIRECTORY_SEPARATOR;
		}
		if(substr($dest_dir,-1,1) != DIRECTORY_SEPARATOR){
			$dest_dir .= DIRECTORY_SEPARATOR;
		}
		
		$dest_path = $dest_dir.$dest_file;
		
		return $dest_file;
	}
	
	function make($file=false,$size_group=false,$size_name=false,$options=false){
		
		$CI = &get_instance();
		
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		$is_rebuild = isset($options['rebuild'] ) && $options['rebuild'] == true;
		$is_overwrite = isset($options['overwrite'] ) && $options['overwrite'] == true;
		$src_dir = isset($options['src'] ) ? $options['src'] : PRV_DATA_DIR.DS.PictureHelper::$default_folder;
		$dest_dir = isset($options['dest'] ) ? $options['dest'] : $src_dir;
		
		$_pos = strrpos($file,'.');
		
		$file_name = substr($file,0,$_pos);
		$file_ext = $_pos >= 0 ? substr($file,$_pos):'';


		if(PictureHelper::$enable_logging){
			log_message('debug','PictureHelper::make, LINE '.__LINE__.': parameters='.json_encode(compact('file','size_group','size_name','options','size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		}

		// if the it given sizetype group name,
		// then we try to load the picture sizetypes group
		$CI->load->helper('data');
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		$CI->load->config('picture');
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		
		$_size_groups = $CI->config->item('picture_sizes');
		$_size_group = NULL;
		$size_info = NULL;
		
		if(is_string($size_group)){
			if(isset($_size_groups[$size_group])){
				$_size_group = $_size_groups[$size_group];
			}
		}elseif(is_array($size_group)){
			$_size_group = $size_group;
		}
		
		if(!$_size_group && isset($size_groups['default'])){
			// default group must always available
			$_size_group = $size_groups['default'];
		}
		
		if(is_string($size_name)){
			if(isset($_size_group[$size_name])){
				$size_info = $_size_group[$size_name];
			}
		}
		
		if(!$size_info && isset($_size_group['default'])){
			// default group must always available
			$size_info = $_size_group['default'];
		}
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		if(!isset($size_info) || empty($size_info)) {
			log_message('error','PictureHelper::make, empty sizetypes. config='.print_r(compact('_size_groups','size_name'),true));
			return NULL;
		}
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		
		$target_name = $file_name;
		if(isset($size_info['prefix'])) $target_name = $size_info['prefix']. $target_name;
		if(isset($size_info['suffix'])) $target_name = $target_name. $size_info['suffix'];
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		$dest_file = $target_name.strtolower($file_ext);;
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		if(substr($src_dir,-1,1) != DIRECTORY_SEPARATOR){
			$src_dir .= DIRECTORY_SEPARATOR;
		}
		if(substr($dest_dir,-1,1) != DIRECTORY_SEPARATOR){
			$dest_dir .= DIRECTORY_SEPARATOR;
		}
		
		$dest_path = $dest_dir.$dest_file;
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		$file_exist = false;
		if(!$file_exist && file_exists($src_dir.$file_name.$file_ext)){
			$file_exist = true;
			$src_path = $src_dir.$file_name.$file_ext;
		}
		if(!$file_exist && file_exists($src_dir.$file_name.strtoupper($file_ext))){
			$file_exist = true;
			$src_path = $src_dir.$file_name.strtoupper($file_ext);
		}
		if(!$file_exist && file_exists($src_dir.$file_name.strtolower($file_ext))){
			$file_exist = true;
			$src_path = $src_dir.$file_name.strtolower($file_ext);
		}
		if(!$file_exist && file_exists($src_dir.strtoupper($file_name.$file_ext))){
			$file_exist = true;
			$src_path = $src_dir.strtoupper($file_name.$file_ext);
		}
		if(!$file_exist && file_exists($src_dir.strtolower($file_name.$file_ext))){
			$file_exist = true;
			$src_path = $src_dir.strtolower($file_name.$file_ext);
		}
		if(!$file_exist && file_exists($src_dir.strtoupper($file_name).$file_ext)){
			$file_exist = true;
			$src_path = $src_dir.strtoupper($file_name).$file_ext;
		}
		if(!$file_exist && file_exists($src_dir.strtolower($file_name).$file_ext)){
			$file_exist = true;
			$src_path = $src_dir.strtolower($file_name).$file_ext;
		}

		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		if(!$file_exist) {
			log_message('error','PictureHelper::make, file (name='.$file_name.',ext='.$file_ext.') does not exist in source path:'.$src_dir);
			return NULL;
		}
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		if(!file_exists($dest_dir)){
			@mkdir($dest_dir,0777);
		}
		if(!is_writable($dest_dir)){
			log_message('error','PictureHelper::make, '.$dest_dir.' not writable');
			return NULL;
		}
		
		//log_message('debug','PictureHelper::make, LINE '.__LINE__.': info='.json_encode(compact('size_info','src_path','dest_path','is_rebuild','is_overwrite')));
		
		if(!file_exists($dest_path) || $is_rebuild || $is_overwrite){
			
			$_options =  array();
			$_options['type'] = 'fit';
			if( isset($size_info['width'])){
				$_options['width'] = $size_info['width'];
			}
			if( isset($size_info['height'])){
				$_options['height'] = $size_info['height'];
			}
			
			if(isset($size_info['crop']) && $size_info['crop']) 
				$_options['crop'] = true;
			if(isset($size_info['scale'] ) ){
				if( $size_info['scale'] == 'fit-fill' || $size_info['scale']  == 'fill'){
					$_options['type'] = 'fill';
				}
			}
			if(isset($size_info['quality'])) 
				$_options['quality'] = $size_info['quality'];
			
			if(!empty($options['croparea'])){
				
				if(PictureHelper::$enable_logging){
					log_message('debug','PictureHelper::path/croparea,'.$options['croparea']);
				}
				try{
					$croparea = NULL;
					if(is_array($options['croparea'])){
						$croparea = $options['croparea'];
					}elseif(is_string($options['croparea'])){
						if(substr($options['croparea'],0,1)=='{'){
							$croparea = json_decode($options['croparea'],true);
						}else{
							list($cx,$cy,$cw,$ch) = explode(',',$options['croparea'],5);
							$croparea['x'] = $cx;
							$croparea['y'] = $cy;
							$croparea['width'] = $cw;
							$croparea['height'] = $ch;
						}
						
					}

					if(isset($croparea[$size_name])){
						
						if(isset($croparea[$size_name]['width'])){
							$_options['crop_x'] = 0;
							$_options['crop_width'] = ($croparea[$size_name]['width']);
						}
						if(isset($croparea[$size_name]['height'])){
							$_options['crop_y'] = 0;
							$_options['crop_height'] = ($croparea[$size_name]['height']);
						}
						if(isset($croparea[$size_name]['x'])){
							$_options['crop_x'] = ($croparea[$size_name]['x']);
						}
						if(isset($croparea[$size_name]['y'])){
							$_options['crop_y'] = ($croparea[$size_name]['y']);
						}
					}elseif(isset($croparea['x']) && isset($croparea['y']) && isset($croparea['width']) && isset($croparea['height'])){
						$_options['crop_x'] = $croparea['x'];
						$_options['crop_y'] = $croparea['y'];
						$_options['crop_width'] = $croparea['width'];
						$_options['crop_height'] = $croparea['height'];
					}
				}catch(Exception $exp){
					
				}
			}

			if(isset($options['crop_x']) && isset($options['crop_y']) && isset($options['crop_width']) && isset($options['crop_height'])){
				$_options['crop_x'] = $options['crop_x'];
				$_options['crop_y'] = $options['crop_y'];
				$_options['crop_width'] = $options['crop_width'];
				$_options['crop_height'] = $options['crop_height'];
			}
			
			PictureHelper::resize( $src_path, $dest_path, $_options);
		}
		
		if(!file_exists($dest_path)){
			log_message('error','PictureHelper::make#no_image_created='.$dest_path);
			return NULL;
		}
		
		return $dest_file;
	}	
}
