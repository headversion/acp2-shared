<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function itemlist_get($sys_name, $is_live='1',$cache_time=3600){
	$CI = &get_instance();
	$CI->load->model('list_model');
	$CI->load->model('list_item_model');
	$CI->load->model('text_locale_model');
	$CI->load->helper('post');
	$CI->load->helper('cache');

	$is_refresh  = $CI->config->item('is_refresh') == true;

	$source_types = $CI->config->item('itemlist_source_types');

	$loc_code = $CI->lang->locale();

	$cache_key = 'itemlist_'.$sys_name.'_'.$loc_code.'';
	$list_row = cache_get($cache_key);

	if((empty($list_row) || $is_refresh) && $cache_time> 0){
		$list_row = $CI->list_model->read(array('_mapping'=> $sys_name, 'is_live'=> $is_live, '_with_locale'=>$loc_code));

		// save into cache file
		if($cache_time>0){
			cache_set($cache_key, $list_row, $cache_time);
		}
	}
	if(empty($list_row['id'])) return NULL;

	$vals = array();
	$vals['id'] = $list_row['id'];
	$vals['title'] = $list_row['loc_title'];
	$vals['child'] = array();


	$cache_key = 'itemlist_'.$sys_name.'_'.$loc_code.'_content';
	$child_rows = cache_get($cache_key);
	if((empty($child_rows) || $is_refresh) && $cache_time> 0){
		$child_rows = $CI->list_item_model->find(array('list_id'=> $list_row['id'],'is_live'=>$is_live,'_order_by'=>array('sequence'=>'asc')));

		// save into cache file
		if($cache_time>0){
			cache_set($cache_key, $child_rows, $cache_time);
		}
	}

	if(is_array($child_rows)){
		foreach($child_rows as $idx => $raw_row){
			if(empty($raw_row['type'])) continue;
			$row = array();
			$row['id'] = $raw_row['id'];
			$row['type'] = $raw_row['type'];
			$row['sequence'] = $raw_row['sequence'];
			$row['ref_table'] = $raw_row['ref_table'];
			$row['ref_id'] = $raw_row['ref_id'];
			$row['url'] = '';
			$row['title'] = '';
			$row['description'] = '';
			$row['parameters'] = $raw_row['parameters'];
			$row['cover_url'] = NULL;

			//if(!empty($source_types[ $raw_row['ref_table']]['label']))
			//	$row['ref_table_str'] = lang($source_types[ $raw_row['ref_table']]['label']);


			if(substr($row['ref_table'],0,3) == 'ph_'){
				$pairs = explode('.',substr($row['ref_table'],3),3);
				if(count($pairs)>=2){
					$type = $pairs[0];
					$section = $pairs[1];

					$ph = PostHelper::get_section($section );
					if(!empty($ph)){
						$model_name = $type.'_model';

						if($type == 'ph_post' && $is_live == '1'){
							$ref_row = $ph->read_post_cache($row['ref_id'], $loc_code);
						}else{
							$options = array('id'=> $row['ref_id'],'is_live'=>'0');
							if($ph->is_locale_enabled) $options['_with_locale'] = $loc_code;
							if(empty($ph->$model_name)) continue;
							$ref_row = $ph->$model_name->read($options);
						}
						if(isset($ref_row['id'])){
							$row['url'] = web_url($CI->lang->locale().'/'.($section == $CI->config->item('ph_section_default') ? '' : $section.'/' ).($type == 'post' ? '' : $type.'/').$ref_row['_mapping']);
							//$row['ref_mapping'] = $ref_row['_mapping'];
							$row['title'] = ($ph->is_locale_enabled) ? $ref_row['loc_title'] : $ref_row['title'];
							$row['description'] = ($ph->is_locale_enabled) ? $ref_row['loc_description'] : $ref_row['description'];
							/*
							if(!empty($ref_row['cover_id'])){
								$row['cover_url'] = site_url('file/'.$ref_row['cover_id'].'/picture?width=50');
							}
							//*/
						}
					}
				}
			}elseif($row['type'] =='custom_link' || $row['type'] =='link'){
				$row['url'] = site_url($raw_row['parameters']->url);
				if(!empty($raw_row['parameters']->loc->$loc_code->title))
					$row['title'] = $raw_row['parameters']->loc->$loc_code->title;
				if(!empty($raw_row['parameters']->loc->$loc_code->description))
					$row['description'] = $raw_row['parameters']->loc->$loc_code->description;
			}

			if(empty($row['description'])){
				$row['description']= '';
			}
			$vals['child'][] = $row;
		}
	}
	return $vals;
}