<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function api_error($code,$message='',$header_code=200,$extra_node=false){
	APIHelper::get_instance()->error($code, $message, $header_code, $extra_node);
}

function api_output($result=false,$raw=false){
	if($raw){
		return APIHelper::get_instance()->raw_output($result);
	}
	APIHelper::get_instance()->output($result);
}

function api_restrict($scopes=false,$allowEmptyAccessToken=false){
	return APIHelper::get_instance()->restrict($scopes,$allowEmptyAccessToken);
}

function api_access_contains($scopes=false)
{
	return APIHelper::get_instance()->access_contains($scopes);
}

function api_token()
{
	return APIHelper::get_instance()->get_token();
}

function api_get_token()
{
	return APIHelper::get_instance()->get_token();
}

class APIHelper
{
	private  $CI = NULL;
	
	private  $session_user = NULL;
	private  $access_token = NULL;
	
	private static $instance = NULL;
	
	public static function get_instance()
	{
		if(!APIHelper::$instance){
			APIHelper::$instance = new APIHelper;
		}
		return APIHelper::$instance;
	}
	
	function __construct(){
		
		GLOBAL $URI;
		
		$CI = &get_instance();
		$this->uri = $CI->uri;
		$this->router = $CI->router;
		$this->load = $CI->load;
		$this->CI = $CI;
		
		if($this->uri->extension() == '') $this->uri->extension('json');
		//$this->load->model('member_model' );

	}

	function get_token(){
		return $this->access_token;
	}

	function restrict($scopes=false,$allowEmptyAccessToken=false){
		
		$CI = $this->CI;
		/*
		if($_SERVER['SERVER_PORT'] != 443){
			
			$this->error(1,'SSL of http request is required.');
			return;
		}
		//*/
		if(is_string($scopes)){
			$scopes = explode(",",$scopes);
		}
		
		if(empty($scopes))
			$scopes = array('basic');
		
		$token = rawurldecode($CI->input->get('access_token'));

		$token = $CI->input->get('access_token');
		if($CI->input->post('access_token')!=NULL){
			$token = $CI->input->post('access_token');
		}
		
		$this->access_token = $token;
		$this->session_user = NULL;
		
		if(empty($token)){
			if($allowEmptyAccessToken){
				return true;
			}
			
			$this->error(121,'Missing required field `access_token`' );
			return false;
		}

		$result = $CI->oauth_auth_server->validate_access_token($token, $scopes);
		if($result == 1){
			$this->error(122,'Your `access_token` is not a valid value or expired.');
			return false;
		}
		if($result == 2){
			$this->error(122,'You are not allowed to perform this request by given `access_token`');
			return false;
		}
		
		
		$user_id = $CI->oauth_auth_server->get_userid($token);
		$user = $CI->member_model->read(array('id'=>$user_id,'is_active'=>1));
		
		if(!isset($user['id'])){
			$this->error(123,'You are not allowed to perform this request by given `access_token`');
			return false;
		}
		$this->session_user = $user;
		
		return true;
	}
	
	function error($code, $message, $status_code=200,$extra_node=false){
		
		
		$CI = $this->CI;
		$friendly = false;
		
		$result = array('error'=>array(
			'code'=>$code,
			'message'=>$message,
			));
		if(!empty($extra_node)){
			$result['detail']=$extra_node;
		}
		
		if(	$this->uri->is_extension('')){
        	return $this->CI->load->view('result',array('output'=>$result));
			//return show_error($message, $status_code);
		}
		
		
		log_message('error','API::Error, '.print_r(compact('code','message','status_code','extra_node'),true));
		
		if(is_int(intval($status_code))) $CI->output->set_status_header(intval($status_code));
		
		
		return $this->output($result);
	}
	
	function access_contains($scopes=array(),$token=false){
		
		if(!$token){
			$token = $this->access_token;
		}
		
		$this->CI->load->library('oauth_auth_server');
		return $this->CI->oauth_auth_server->validate_access_token($token, $scopes);
	}
	
    function output($vals, $opts=false){
            
        $this->CI->load->helper('data');
		
		if(method_exists($this->CI, '_is_debug') && $this->CI->_is_debug()){
			if(is_array($vals)){
				
				if(isset($this->CI->db->queries))
					$vals['queries'] = $this->CI->db->queries;
			}
		}
        
        if(empty($opts) || !is_array($opts))
            $opts = array();
        
        if($this->CI->uri->is_extension('json') ){
			
		
		
            $opts = array();
            $callback = $this->CI->input->get('callback');
            if($callback!=null && strlen($callback)>0)
                $opts['callback'] = $callback;
                
            json_output($vals,false,$opts);
        }elseif($this->CI->uri->is_extension('xml') ){
            xml_output($vals,false,$opts);
        }elseif($this->CI->uri->is_extension('plist') ){
            plist_output($vals,false,$opts);
        }else{
		
            $callback = $this->CI->input->get('jscallback');
            if($callback!=null && strlen($callback)>0){
				print '<html><body><script>'.$callback.'('.json_encode($vals).');</script></body></html>';
				return;
			}
		
            $this->CI->load->view('result',array('output'=>$vals));
        }
    }
}