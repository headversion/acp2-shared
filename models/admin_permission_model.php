<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Admin_permission_model extends LMS_Model {
	var $table = 'admin_permissions';

	// we already check this data for ensuring what should be handled
	var $fields = array('id', 'name', 'key', 'create_date', 'create_by_id', 'modify_date', 'modify_by_id');

	var $auto_increment = true;
	var $fields_details = array(
		'id' => array(
			'type'           => 'BIGINT',
			'constraint'     => 20,
			'auto_increment' => TRUE
		),
		'name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'key' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);

	// data validation, should be called before calling save function
	// @return {Dictionary}, Elements contain 'success'{Boolean} value of validation, 'fields'{Array} name of highlighted fields, and 'messages'{Dictionary} key of the message/issue.
	function validate($data) {

		$success = true;
		$fields  = array();
		$issues  = array();

		if (isset($data['name'])) {
			if (strlen($data['name']) < 1) {
				$success              = false;
				$fields['name']       = true;
				$issues['name_empty'] = true;
			}
		}

		if (isset($data['key'])) {
			if (!preg_match("/^[a-zA-Z0-9\.\-\_]{4,40}$/", $data['key'])) {
				$success                      = false;
				$fields['key']                = true;
				$issues['key_format_invalid'] = true;
			}
		}

		return compact('success', 'fields', 'issues');
	}
}
