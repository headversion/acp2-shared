<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Admin_role_model extends LMS_Model {
	var $table  = 'admin_roles';
	var $fields = array('id', 'name', 'sys_name', 'create_date', 'create_by_id', 'modify_date', 'modify_by_id');

	var $default_values = array('name' => '', 'sys_name' => '');
	var $auto_increment = true;
	var $fields_details = array(
		'id' => array(
			'type'           => 'BIGINT',
			'constraint'     => 20,
			'auto_increment' => TRUE
		),
		'name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'sys_name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);

	// Admin role's Permissions relationship
	var $perms_table          = 'admin_roles_permissions';
	var $perms_fields_details = array(
		'role_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
			'pk'         => TRUE,
		),
		'permission_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
			'pk'         => TRUE,
		),
		'value' => array(
			'type' => 'TEXT',
			'null' => TRUE,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);

	public function install() {
		parent::install();

		if ($this->auto_install && !empty($this->perms_table) && !empty($this->perms_fields_details)) {
			$this->install_table($this->perms_table, array_keys($this->perms_fields_details), $this->perms_fields_details);
		}
	}

	public function uninstall() {
		parent::uninstall();
		if ($this->auto_install && !empty($this->perms_table) && !empty($this->perms_fields_details)) {
			$this->load->dbforge();
			$this->dbforge->drop_table($this->perms_table);
		}
	}

	function get_permission_ids($role_id, $options = false) {

		$perms_table = $this->table($this->perms_table);

		$this->db->where('role_id', $role_id);
		$this->db->order_by('create_date', 'asc');
		$query = $this->db->get($perms_table);

		if (!$query || $query->num_rows() < 1) {
			return NULL;
		}
		return $this->result_array();
	}

	function add_permission($role_id, $permission_id, $value = 1, $options = false) {
		if (empty($admin_id) || empty($permission_id)) {
			log_message('error', get_called_class() . '/' . __METHOD__ . ', passed with empty required value: $role_id="' . $role_id . '", $admin_idpermission_id="' . $permission_id . '"');
			return FALSE;
		}

		$sql_data = compact('role_id', 'permission_id', 'value');

		$this->save_pre_data_attr($sql_data, true, $options);

		$perms_table = $this->table($this->perms_table);
		$this->db->insert($perms_table, $sql_data);

		return TRUE;
	}

	function remove_permission($role_id, $permission_id) {

		if (empty($admin_id) || empty($permission_id)) {
			log_message('error', get_called_class() . '/' . __METHOD__ . ', passed with empty required value: $role_id="' . $role_id . '", $permission_id="' . $permission_id . '"');
			return FALSE;
		}

		$perms_table = $this->table($this->perms_table);
		$this->db->where('role_id', $admin_id)->where('permission_id', $permission_id);
		$this->db->delete($perms_table);

		return TRUE;
	}

	// data validation, should be called before calling save function
	// @return {Dictionary}, Elements contain 'success'{Boolean} value of validation, 'fields'{Array} name of highlighted fields, and 'messages'{Dictionary} key of the message/issue.
	function validate($data) {

		$success = true;
		$fields  = array();
		$issues  = array();

		if (isset($data['name'])) {
			if (strlen($data['name']) < 1) {
				$success              = false;
				$fields['name']       = true;
				$issues['name_empty'] = true;
			}
		}

		if (isset($data['sys_name'])) {
			if (!preg_match("/^[a-zA-Z][a-zA-Z0-9\.\-\_]{3,39}$/", $data['sys_name'])) {
				$success                           = false;
				$fields['sys_name']                = true;
				$issues['sys_name_format_invalid'] = true;
			}
		}

		return compact('success', 'fields', 'issues');
	}
}
