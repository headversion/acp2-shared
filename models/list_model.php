<?php

class List_model extends LMS_Model {
	var $table  = 'lists';
	var $fields = array('id',
		'title',
		'sys_name',
		'default_locale',

		'sort_type',
		'display_limit',
		'description',
		'content',
		'start_date',
		'end_date',

		'status',
		'is_live',
		'is_pushed',
		'last_pushed',

		'create_date',
		'create_by_id',
		'modify_date',
		'modify_by_id',
	);

	var $locale_table = 'text_locales';

	var $table_indexes = array('sys_name','default_locale');

	var $fields_details = array(
		'id' => array(
			'type'           => 'BIGINT',
			'constraint'     => 20,
			'pk'             => TRUE,
			'auto_increment' => TRUE
		),
		'sys_name' => array(
			'type'       => 'VARCHAR',
			'constraint' => '50',
		),
		'default_locale' => array(
			'type'       => 'VARCHAR',
			'constraint' => '5',
		),

		'title' => array(
			'type'       => 'VARCHAR',
			'constraint' => '200',
		),
		'description' => array(
			'type' => 'TEXT',
			'null' => TRUE,
		),
		'content' => array(
			'type' => 'TEXT',
			'null' => TRUE,
		),

		'sort_type' => array(
			'type'       => 'VARCHAR',
			'constraint' => '36',
			'default'    => 'sequence',
		),
		'display_limit' => array(
			'type'       => 'INT',
			'constraint' => 10,
		),

		'start_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'end_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),

		'status' => array(
			'type'       => 'INT',
			'constraint' => 1,
		),
		'is_live' => array(
			'type'       => 'INT',
			'pk'         => TRUE,
			'constraint' => 1,
		),
		'is_pushed' => array(
			'type'       => 'INT',
			'constraint' => 1,
		),
		'last_pushed' => array(
			'type'       => 'DATETIME',
			'null'		 => TRUE,
		),

		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);



	protected function selecting_options($options = false, $cache = false) {

		$locale_table = $this->locale_table;
		
		// load locale text content (LEFT JOIN)
		if (!empty($options['_with_locale'])) {
			if (isset($options['_keyword_fields'])) {
				$options['_keyword_fields'][] = $locale_table . '.title';
				$options['_keyword_fields'][] = $locale_table . '.parameters';
			}
		}

		parent::selecting_options($options, $cache);
		// load locale text content (LEFT JOIN)
		if (!empty($options['_with_locale'])) {

			$prefix = isset($options['_with_locale_prefix']) ? $options['_with_locale_prefix'] : 'loc_';

			$this->db->select($this->table.'.*,' .
				$locale_table . '.title as '.$prefix.'title,' .
				$locale_table . '.parameters as '.$prefix.'parameters,' .
				$locale_table . '.locale as locale'
			);
			$this->db->join($locale_table,
				$this->table . '.id = ' . $this->locale_table . '.ref_id '
				. 'AND ' . $this->table . '.is_live = ' . $this->locale_table . '.is_live '
				. 'AND ' . $this->locale_table . '.ref_table = \'' . $this->db->escape_str($this->table) . '\' ',
				'LEFT');
			$this->_where_match('locale', $options['_with_locale']);

		}
	}
}
