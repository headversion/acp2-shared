<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class Admin_model extends LMS_Model {
	var $table = 'admins';

	var $fields = array('id', 'login_name', 'login_pass', 'name', 'email', 'is_active', 'last_access', 'create_date', 'create_by_id', 'modify_date', 'modify_by_id');

	//var $default_values = array('login_name'=>'','login_pass'=>'','name'=>'','email'=>'','is_active'=>1,'last_access'=>NULL,);
	var $auto_increment = true;
	var $fields_details = array(
		'id' => array(
			'type'           => 'BIGINT',
			'constraint'     => 20,
			'auto_increment' => TRUE
		),
		'login_name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'login_pass' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'email' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'is_active' => array(
			'type'       => 'INT',
			'constraint' => 1,
		),
		'last_access' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);

	public function install() {
		parent::install();

		if ($this->auto_install && !empty($this->perms_table) && !empty($this->perms_fields_details)) {
			$this->install_table($this->perms_table, array_keys($this->perms_fields_details), $this->perms_fields_details);
		}
		if ($this->auto_install && !empty($this->role_table) && !empty($this->role_fields_details)) {
			$this->install_table($this->role_table, array_keys($this->role_fields_details), $this->role_fields_details);
		}
	}

	public function uninstall() {
		parent::uninstall();
		if ($this->auto_install && !empty($this->perms_table) && !empty($this->perms_fields_details)) {
			$this->load->dbforge();
			$this->dbforge->drop_table($this->perms_table);
		}
		if ($this->auto_install && !empty($this->role_table) && !empty($this->role_fields_details)) {
			$this->load->dbforge();
			$this->dbforge->drop_table($this->role_table);
		}
	}

	public function validate($data, $options) {
		$success = true;
		$fields  = array();
		$issues  = array();

		if (isset($data['login_name'])) {
			if (strlen($data['login_name']) < 4) {
				$success                        = false;
				$fields['login_name']           = TRUE;
				$issues['login_name_too_short'] = true;
			} elseif (strlen($data['login_name']) > 40) {
				$success                       = false;
				$fields['login_name']          = TRUE;
				$issues['login_name_too_long'] = true;
			} elseif (!preg_match("/^[a-zA-Z].+$/", $data['login_name'])) {
				$success                               = false;
				$fields['login_name']                  = TRUE;
				$issues['login_name_not_letter_first'] = true;
			}
		}

		if (isset($data['login_pass']) && !empty($data['login_pass'])) {
			if (strlen($data['login_pass']) < 8) {
				$success                        = false;
				$fields['login_pass']           = TRUE;
				$issues['login_pass_too_short'] = true;
			} elseif (strlen($data['login_pass']) > 32) {
				$success                       = false;
				$fields['login_pass']          = TRUE;
				$issues['login_pass_too_long'] = true;

			} elseif (preg_match("/^[0-9].+$/", $data['login_pass'])) {
				$success                                   = false;
				$fields['login_pass']                      = TRUE;
				$issues['login_pass_cannot_numeric_first'] = true;

			} elseif (isset($data['login_retype_pass'])) {
				if ($data['login_pass'] != $data['login_retype_pass']) {

					$success                            = false;
					$fields['login_retype_pass']        = TRUE;
					$issues['login_retype_not_matched'] = true;
				}
			}
		}

		if (isset($data['email'])) {
			if (strlen($data['email']) < 4) {
				$success                   = false;
				$fields['email']           = TRUE;
				$issues['email_too_short'] = true;
			} elseif (!preg_match("/^[a-zA-Z-_\.]+\@[a-zA-Z-_\.]+[a-zA-Z0-9]+$/", $data['email'])) {
				$success                     = false;
				$fields['email']             = TRUE;
				$issues['email_not_correct'] = true;
			}
		}

		return compact('success', 'fields', 'issues');
	}

	// Admin's Permissions relationship
	var $perms_table          = 'admins_permissions';
	var $perms_fields_details = array(
		'admin_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
			'pk'         => TRUE
		),
		'permission_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
			'pk'         => TRUE
		),
		'value' => array(
			'type' => 'TEXT',
			'null' => TRUE,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);

	function get_permission_ids($admin_id, $options = false) {

		$perms_table = $this->_table(NULL, $this->perms_table);

		$this->db->where('admin_id', $admin_id);
		$this->db->order_by('create_date', 'asc');
		$query = $this->db->get($perms_table);

		if (!$query || $query->num_rows() < 1) {
			return NULL;
		}
		$ids = array();
		$num = $query->num_rows();
		$row = $query->first_row('array');
		for ($i = 0; $i < $num; $i++) {
			$ids[] = $row['permission_id'];
			$row   = $query->next_row('array');
		}
		return $ids;
	}

	function get_permissions($admin_id, $options = false) {

		$perms_table = $this->_table(NULL, $this->perms_table);

		$this->db->where('admin_id', $admin_id);
		$this->db->order_by('create_date', 'asc');
		$query = $this->db->get($perms_table);

		if (!$query || $query->num_rows() < 1) {
			return NULL;
		}
		$rows = $query->result_array();
		return $rows;
	}

	function get_permission($admin_id, $perm_id, $options = false) {

		$perms_table = $this->_table(NULL, $this->perms_table);

		$this->db->where('admin_id', $admin_id);
		$this->db->where('permission_id', $perm_id);
		$query = $this->db->get($perms_table);

		if (!$query || $query->num_rows() < 1) {
			return NULL;
		}
		$ids = array();
		$num = $query->num_rows();
		$row = $query->first_row('array');
		return $row;
	}

	function add_permission($admin_id, $permission_id, $value = 1, $options = false) {
		if (empty($admin_id) || empty($permission_id)) {
			log_message('error', get_called_class() . '/' . __METHOD__ . ', passed with empty required value: $admin_id="' . $admin_id . '", $admin_idpermission_id="' . $permission_id . '"');
			return FALSE;
		}

		$sql_data = compact('admin_id', 'permission_id', 'value');

		$this->save_pre_data_attr($sql_data, true, $options);

		$perms_table = $this->_table(NULL, $this->perms_table);
		$this->db->insert($perms_table, $sql_data);

		return TRUE;
	}

	function remove_permission($admin_id, $permission_id) {

		if (empty($admin_id) || empty($permission_id)) {
			log_message('error', get_called_class() . '/' . __METHOD__ . ', passed with empty required value: $admin_id="' . $admin_id . '", $permission_id="' . $permission_id . '"');
			return FALSE;
		}

		$perms_table = $this->table($this->perms_table);
		$this->db->where('admin_id', $admin_id)->where('permission_id', $permission_id);
		$this->db->delete($perms_table);

		return TRUE;
	}

	// Admin's Roles relationship
	var $role_table          = 'admins_roles';
	var $role_fields_details = array(
		'id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
			'pk'         => TRUE
		),
		'admin_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'role_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'is_active' => array(
			'type'       => 'INT',
			'constraint' => 1,
		),
		'start_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'end_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);

	function get_roles($admin_id, $options = false) {

		$role_table = $this->_table(NULL, $this->role_table);

		if (isset($options['is_active'])) {
			$this->db->where('is_active', $options['is_active']);
		}

		if (isset($options['_range_on'])) {
			$_val  = $options['_range_on'] == 'now' ? time_to_date() : $options['_range_on'];
			$conds = array();

			$_field = $role_table . '.' . $this->_field('start_date');

			$conds[] = $_field . '<= ' . $this->db->escape_str($_val);
			$conds[] = $_field . ' IS NULL';
			$this->_or($conds);

			$_field = $role_table . '.' . $this->_field('end_date');

			$conds[] = $_field . ' > ' . $this->db->escape_str($_val);
			$conds[] = $_field . ' IS NULL';
			$this->_or($conds);
		}

		$this->db->where('admin_id', $admin_id);
		$this->db->order_by('create_date', 'asc');
		$query = $this->db->get($role_table);

		if (!$query || $query->num_rows() < 1) {
			return NULL;
		}
		return $query->result_array();
	}

	function get_role($options = false) {

		if (isset($options['is_active'])) {
			$this->db->where('is_active', $options['is_active']);
		}

		$role_table = $this->_table(NULL, $this->role_table);

		$this->db->where('id', $relation_id);
		$query = $this->db->get($role_table);
		if (!$query || $query->num_rows() != 1) {
			return NULL;
		}

		return $query->row_array();
	}

	function add_role($sql_data, $options = false) {
		if (empty($sql_data['admin_id']) || empty($sql_data['role_id'])) {
			log_message('error', get_called_class() . '/' . __METHOD__ . ', passed with empty required value: $admin_id="' . $sql_data['admin_id'] . '", $role_id="' . $sql_data['role_id'] . '"');
			return NULL;
		}

		$this->save_pre_data_attr($sql_data, true, $options);

		$role_table = $this->_table(NULL, $this->role_table);
		$this->db->insert($role_table, $sql_data);

		//log_message('debug',get_called_class() .'/'.__METHOD__.', '.$this->db->last_query());

		$relation_id = $this->db->insert_id();

		return array('id' => $relation_id);
	}

	function update_role($relation_id, $sql_data, $options = false) {
		$arg0 = func_get_arg(1);// second parameter
		if (is_array($arg0) && isset($arg0['admin_id'])) {
			extract($arg0);
		}
		if (empty($relation_id) || empty($sql_data['role_id'])) {
			log_message('error', get_called_class() . '/' . __METHOD__ . ', passed with empty required value: $relation_id="' . $relation_id . '"');
			return NULL;
		}

		// time_to_date is one of helper function in 'datetime_helper.php' that configured with time-zone
		$modify_date = time_to_date();

		$this->save_pre_data_attr($sql_data, false, $options);

		$role_table = $this->_table(NULL, $this->role_table);

		$this->db->where('id', $relation_id);
		$this->db->update($role_table, $sql_data);

		//log_message('debug',get_called_class() .'/'.__METHOD__.', '.$this->db->last_query());

		return array('id' => $relation_id);
	}

	function remove_role($relation_id) {

		if (empty($relation_id)) {
			log_message('error', get_called_class() . '/' . __METHOD__ . ', passed with empty required value: $relation_id="' . $relation_id . '"');
			return FALSE;
		}
		$role_table = $this->_table(NULL, $this->role_table);
		$this->db->where($role_table . '.id', $relation_id);
		$this->db->delete($role_table);

		return TRUE;
	}
}
