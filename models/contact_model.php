<?php
class Contact_model extends LMS_Model {
	var $table  = 'contact';
	var $fields = array('id', 'name', 'email', 'subject', 'content',
		'is_view',
		'is_reply', 'reply_name', 'reply_email', 'reply_content', 'reply_date',
		'create_date', 'modify_date', 'modify_by', 'modify_by_id');
	var $default_values = array(
		'name'          => NULL,
		'email'         => NULL,
		'subject'       => NULL,
		'content'       => NULL,
		'is_view'       => 0,
		'is_reply'      => 0,
		'reply_name'    => NULL,
		'reply_email'   => NULL,
		'reply_content' => NULL,
		'reply_date'    => NULL,
		'create_date'   => NULL,
		'modify_date'   => NULL,
	);
	var $auto_increment = true;
}
